﻿using ForumSystem.Data.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Data.Models.Mappers
{
    public class CommentMapper
    {
        
        public Comment ConvertToModel(CreateCommentDto commentDto)
        {
            if (commentDto == null)
            {
                throw new ArgumentNullException("CommentDto can not be null!");
            }

            var comment = new Comment();
            comment.Content = commentDto.Content;
            comment.PostId = commentDto.PostId;

            return comment;
        }

        public Comment ConverToModel(int id, User user, UpdateCommentDto commentDto)
        {
            if (commentDto == null)
            {
                throw new ArgumentNullException("CommentDto can not be null!");
            }
            var comment = new Comment();
            comment.Id = id;
            comment.Content = commentDto.Content;
            comment.UserId = user.Id;
            return comment;
        }

        public Comment ConvertToModel(User user, CommentsModel viewModel)
        {
            var comment = new Comment();
            comment.Content = viewModel.Content;
            comment.User = user;
            comment.UserId = user.Id;
            comment.Post = viewModel.Post;
            comment.PostId = viewModel.Post.Id;

            return comment;
        }

        public ResponseCommentDto ConvertToDto(Comment comment)
        {
            if (comment == null)
            {
                throw new ArgumentNullException("Comment does not exist!");
            }

            var responseDto = new ResponseCommentDto();

            responseDto.Content = comment.Content;
            responseDto.PostTitle = comment.Post.Title;
            responseDto.UserName = comment.User.Username;

            return responseDto;
        }
    }
}
