﻿using System.Linq;

namespace ForumSystem.Data.Models.Mappers
{
    public class UserMapper
    {
        public User ConvertToUser(UserDto newUser)
        {
            var createUser = new User
            {
                Username = newUser.Username,
                Password = newUser.Password,
                FirstName = newUser.FirstName,
                LastName = newUser.LastName,
                Email = newUser.Email
            };

            return createUser;
        }

        public UserResponseDto ConvertToResponseUser(User createdUser)
        {
            UserResponseDto newUserResponse = new UserResponseDto
            {
                FirstName = createdUser.FirstName,
                LastName = createdUser.LastName,
                Email = createdUser.Email,
                Username = createdUser.Username,
                Posts = createdUser.Posts.Count()
            };

            return newUserResponse;
        }
    }
}
