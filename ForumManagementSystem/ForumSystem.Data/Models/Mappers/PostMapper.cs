﻿using ForumSystem.Data.Models.Dto_s;
using ForumSystem.Data.Models.ViewModels;
using System;

namespace ForumSystem.Data.Models.Mappers
{
    public class PostMapper
    {
        public Post ConvertToPost(User user, PostDto postDto)
        {
            if (postDto == null)
            {
                throw new ArgumentNullException("PostDto can not be null!");
            }

            var post = new Post
            {
                Title = postDto.Title,
                Content = postDto.Content,
                User = user,
                UserId = user.Id
            };
            return post;
        }
        
        public Post ConvertToPost(User user, UpdatePostDto updatePostDto)
        {
            if (updatePostDto == null)
            {
                throw new ArgumentNullException("UpdatePostDto can not be null!");
            }

            var post = new Post
            {
                Title = updatePostDto.Title,
                Content = updatePostDto.Content,
                User = user,
                UserId = user.Id
            };
            return post;
        }

        public PostDto ConvertToPostDto(UpdatePostDto updatePostDto, Post post)
        {
            if (updatePostDto == null)
            {
                throw new ArgumentNullException("UpdatePostDto can not be null!");
            }

            var responsePostDto = new PostDto
            {
                Title = updatePostDto.Title ?? post.Title,
                Content = updatePostDto.Content ?? post.Content
            };
            return responsePostDto;
        }

        public PostDto ConvertToPostDto(Post post)
        {
            var postDto = new PostDto
            {
                Title = post.Title,
                Content = post.Content
            };

            return postDto;
        }

        public PostResponseDto ConvertToResponseDto(Post post)
        {
            var responseDto = new PostResponseDto
            {
                Title = post.Title,
                Content = post.Content,
                Username = post.User.Username,
                Likes = post.Likes
            };

            foreach (var tag in post.Tags)
            {
                responseDto.Tags.Add(tag.Tag.TagContent);
            }

            return responseDto;
        }
    }
}
