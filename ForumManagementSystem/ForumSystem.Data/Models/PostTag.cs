﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Data.Models
{
    public class PostTag
    {
        [Key]
        public int Id { get; set; }

        public int? TagId { get; set; }

        public Tag Tag { get; set; }
        
        public int PostId { get; set; }

        public Post Post { get; set; }
    }
}
