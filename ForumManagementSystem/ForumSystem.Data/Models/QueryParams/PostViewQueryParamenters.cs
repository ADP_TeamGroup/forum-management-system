﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Data.Models.QueryParams
{
    public class PostViewQueryParamenters
    {
        public string Title { get; set; }
        public string Username { get; set; }
        public string Tag { get; set; }
    }
}
