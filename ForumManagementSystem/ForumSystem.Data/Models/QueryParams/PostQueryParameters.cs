﻿namespace ForumSystem.Data.Models
{
    public class PostQueryParameters
    {
        public string Username { get; set; }
        public string SortBy { get; set; }
        public string Tag { get; set; }
    }
}
