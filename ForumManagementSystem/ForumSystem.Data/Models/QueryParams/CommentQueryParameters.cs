﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Data.Models.Mappers
{
    public class CommentQueryParameters
    {
        public int PostId { get; set; }

        public string PostTitle { get; set; }

        //public bool IsEmpty()
        //{
        //    return PostId &&
        //           string.IsNullOrWhiteSpace(PostTitle);
        //}
    }
}
