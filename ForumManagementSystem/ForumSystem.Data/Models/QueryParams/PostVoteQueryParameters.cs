﻿namespace ForumSystem.Data.Models
{
    public class PostVoteQueryParameters
    {
        public string Vote { get; set; }
    }
}
