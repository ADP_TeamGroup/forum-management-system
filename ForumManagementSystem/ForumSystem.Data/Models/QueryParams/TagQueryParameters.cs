﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Data.Models.QueryParams
{
    public class TagQueryParameters
    {
        public string Tag { get; set; }
    }
}
