﻿namespace ForumSystem.Data.Models
{
    public class UserQuerryParameters
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public bool IsEmpty()
        {
            return string.IsNullOrWhiteSpace(Username) &&
                   string.IsNullOrWhiteSpace(Email) &&
                   string.IsNullOrWhiteSpace(FirstName) &&
                   string.IsNullOrWhiteSpace(LastName);
        }
    }
}
