﻿using ForumSystem.Data.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace ForumSystem.Data.Models
{
    public class PostLike
    {
        [Key]
        public int Id { get; set; }
        public int? PostId { get; set; }
        public Post Post { get; set; }
        public int? UserId { get; set; }
        public User User { get; set; }
        public Reaction Vote { get; set; }
    }
}
