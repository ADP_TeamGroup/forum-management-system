﻿namespace ForumSystem.Data.Models.Enums
{
    public enum Reaction
    {
        Dislike = -1,
        Neutral = 0,
        Like = 1,
    }
}
