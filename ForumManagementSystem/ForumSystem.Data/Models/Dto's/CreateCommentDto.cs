﻿using System.ComponentModel.DataAnnotations;

namespace ForumSystem.Data.Models
{
    public class CreateCommentDto
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} field is required and must not be an empty string.")]
        public string Content { get; set; }

        [Range (1, int.MaxValue, ErrorMessage = "{0} is required!")]
        
        public int PostId { get; set; }
    }
}
