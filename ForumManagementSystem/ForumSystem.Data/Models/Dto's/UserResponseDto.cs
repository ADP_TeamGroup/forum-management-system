﻿namespace ForumSystem.Data.Models
{
    public class UserResponseDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public int Posts { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as UserResponseDto;

            if (item == null)
            {
                return false;
            }

            return this.FirstName == item.FirstName && 
                    this.LastName == item.LastName && 
                     this.Email == item.Email && 
                      this.Username == item.Username;
        }

        public override int GetHashCode()
        {
            return this.FirstName.GetHashCode() ^
                    this.LastName.GetHashCode() ^
                     this.Email.GetHashCode() ^
                      this.Username.GetHashCode();
        }
    }
}
