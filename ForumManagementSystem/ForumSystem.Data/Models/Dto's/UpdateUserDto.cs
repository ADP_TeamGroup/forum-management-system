﻿using System.ComponentModel.DataAnnotations;

namespace ForumSystem.Data.Models
{
    public class UpdateUserDto
    {
        [StringLength(32, MinimumLength = 4, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string FirstName { get; set; }

        [StringLength(32, MinimumLength = 4, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsBlocked { get; set; }
    }
}
