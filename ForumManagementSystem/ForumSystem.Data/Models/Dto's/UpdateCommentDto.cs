﻿using System.ComponentModel.DataAnnotations;

namespace ForumSystem.Data.Models
{
   public class UpdateCommentDto
    {
       
        [Required(AllowEmptyStrings = false, ErrorMessage = "The {0} field is required and must not be an empty string.")]
        public string Content { get; set; }


    }
}
