﻿using System.Collections.Generic;

namespace ForumSystem.Data.Models.Dto_s
{
    public class PostResponseDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Username { get; set; }
        public int Likes { get; set; }
        public List<string> Tags { get; set; } = new List<string>();
    }
}
