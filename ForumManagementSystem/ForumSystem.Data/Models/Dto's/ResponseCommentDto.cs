﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Data.Models
{
    public class ResponseCommentDto
    {
       
        public string Content { get; set; }

        public string PostTitle { get; set; }

        public string UserName { get; set; }


        public override bool Equals(object obj)
        {
            var item = obj as ResponseCommentDto;

            if (item == null)
            {
                return false;
            }

            return this.Content == item.Content &&
                    this.PostTitle == item.PostTitle &&
                     this.UserName == item.UserName;
        }

        public override int GetHashCode()
        {
            return this.Content.GetHashCode() ^
                    this.PostTitle.GetHashCode() ^
                     this.UserName.GetHashCode();
        }
    }
}
