﻿using System.ComponentModel.DataAnnotations;

namespace ForumSystem.Data.Models
{
    public class UserDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(32, MinimumLength = 4, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(32, MinimumLength = 4, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string LastName { get; set; }
    }
}
