﻿using System.ComponentModel.DataAnnotations;

namespace ForumSystem.Data.Models.Dto_s
{
     public class UpdatePostDto
    {
        [StringLength(64, MinimumLength = 16, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string Title { get; set; }

        [StringLength(8192, MinimumLength = 32, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string Content { get; set; }

        [StringLength(25,MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string Tag { get; set; }
    }
}
