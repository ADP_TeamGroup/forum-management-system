﻿using System.ComponentModel.DataAnnotations;

namespace ForumSystem.Data.Models
{
    public class PostDto
    {
        [Required]
        [StringLength(64, MinimumLength = 16, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string Title { get; set; }

        [Required]
        [StringLength(8192,MinimumLength = 32 , ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string Content { get; set; }
    }
}
