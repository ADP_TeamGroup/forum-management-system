﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ForumSystem.Data.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        
        public int PostId { get; set; }
        
        public Post Post { get; set; }
        
        public int? UserId { get; set; }

        public User User { get; set; }

    }
}
