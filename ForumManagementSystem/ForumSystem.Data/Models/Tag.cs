﻿using System.ComponentModel.DataAnnotations;


namespace ForumSystem.Data.Models
{
    public class Tag
    {
        [Key]
        public int Id { get; set; }

        public string TagContent { get; set; }
    }
}
