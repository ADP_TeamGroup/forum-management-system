﻿

using System.Collections.Generic;

namespace ForumSystem.Data.Models.ViewModels
{
    public class PostsIndexModel
    {
       public IEnumerable<Post> Posts { get; set; }

        public Dictionary<int, List<string>> PostTagsContent { get; set; }
    }
}
