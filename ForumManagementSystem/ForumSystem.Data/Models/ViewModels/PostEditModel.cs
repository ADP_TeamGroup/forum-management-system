﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Data.Models.ViewModels
{
    public class PostEditModel
    {
        public int Id { get; set; }

        [StringLength(64, MinimumLength = 16, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string Title { get; set; }

        [StringLength(8192, MinimumLength = 32, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string Content { get; set; }

        [StringLength(25, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} symbols long!")]
        public string Tag { get; set; }

        public Post Post { get; set; }
    }
}
