﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Data.Models.ViewModels
{
    public class CommentsModel
    {
        public IEnumerable<Comment> Comments { get; set; }
        public Comment Comment { get; set; }
        public int CommentId { get; set; }
        public Post Post { get; set; }
        public int PostId { get; set; }
        public string Content { get; set; }
        public string Username { get; set; }

    }
}
