﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ForumSystem.Data.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; } 

        [Required]
        [Range(16,64,ErrorMessage = "{0} must be between {1} and {2} symbols long!")]
        public string Title { get; set; }

        [Required]
        public string Content { get; set; }

        public int Likes { get; set; }

        public List<PostLike> Votes { get; set; } = new List<PostLike>();

        public List<PostTag> Tags { get; set; } = new List<PostTag>();

        public int UserId { get; set; }

        public User User { get; set; }

        public List<Comment> Comments { get; set; } = new List<Comment>();

        public override bool Equals(object obj)
        {
            if (obj is not Post item)
            {
                return false;
            }

            return this.Id == item.Id;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}
