﻿using ForumSystem.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace ForumSystem.Data
{
    public class ForumDbContext : DbContext
    {
        public ForumDbContext(DbContextOptions<ForumDbContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }

        public DbSet<Post> Posts { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<PostLike> PostLikes { get; set; }

        public DbSet<PostTag> PostTags { get; set; }

        public DbSet<Tag> Tags { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            var user = new User
            {
                Id = 1,
                FirstName = "Gosho",
                LastName = "Ivanov",
                Email = "G_Ivanov@gosho.com",
                Username = "GoshoAdmin",
                Password = "GoshoAdmin",
                IsAdmin = true,
                PhoneNumber = "0891234567"
            };
            modelBuilder.Entity<User>().HasData(user);

            /*            modelBuilder.Entity<Post>()
                            .HasMany<Tag>(s => s.Tags)
                            .WithMany(c => c.Posts);*/

            /*            modelBuilder
                            .Entity<User>()
                            .HasMany(user => user.Posts)
                            .WithOne(post => post.User)
                            .HasForeignKey(post => post.UserId)
                            .OnDelete(DeleteBehavior.SetNull);

                        modelBuilder
                            .Entity<User>()
                            .HasMany(user => user.Comments)
                            .WithOne(post => post.User)
                            .HasForeignKey(post => post.UserId)
                            .OnDelete(DeleteBehavior.SetNull);*/
        }
    }
}
