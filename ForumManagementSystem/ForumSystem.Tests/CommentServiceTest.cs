using Microsoft.VisualStudio.TestTools.UnitTesting;
using ForumSystem.Data;
using Microsoft.EntityFrameworkCore;
using ForumSystem.Services;
using ForumSystem.Data.Models.Mappers;
using Moq;
using System.Collections.Generic;
using ForumSystem.Data.Models;
using ForumSystem.Services.Exceptions;
using System.Linq;

namespace ForumSystem.Tests
{
    [TestClass]
    public class CommentServiceTest
    {

        private readonly ForumDbContext _context;
        private readonly CommentService _sut;
        private readonly Mock<CommentMapper> _commentMapper = new();

        public CommentServiceTest()
        {
            _context = GetInMemoryContext();
            _sut = new CommentService(_context, _commentMapper.Object);

            SeedDatabase();
        }

        //        ---------------------------------------------------------------------------------     Clean context after every test
        [TestCleanup]
        public void CleanUp()
        {
            _context.Database.EnsureDeleted();
        }



     

        [TestMethod]
        [ExpectedException(typeof(CommentNotFoundException), "Comment does not exist!")]
        public void Get_ShouldThrow_WhenCommentNotFound()
        {
            // Arrange
            var user = _context.Users.FirstOrDefault(u => u.Id == 1);
            // Act, Assert
            _sut.Get(user, 10);
        }

        [TestMethod]
        public void Get_ShouldReturn_WhenValidId()
        {
            // Arrange
            var user = _context.Users.FirstOrDefault(u => u.Id == 1);
            // Act
            var comment = _sut.Get(user, 1);
            var expected = _context.Comments.First();

            // Assert
            Assert.AreEqual(expected, comment);
        }




        //        ---------------------------------------------------------------------------------     Tests for Create
        

        [TestMethod]
        [ExpectedException(typeof(UserAlreadyBlockedUnblocked), "User is blocked!")]
        public void CreateComment_ShouldThrow_WhenUserIsBlocked()
        {
            // Arrange
            var user = _context.Users.FirstOrDefault(u => u.Id == 3);

            // Act, Assert
            _sut.Create(user, new CreateCommentDto());
        }


        [TestMethod]
        public void CreateComment_ShouldReturnProperDto_WhenValid()
        {
            // Arrange
            var commentToCreate = new CreateCommentDto
            {
                Content = "I don't enjoy going to Space!",
                PostId = 4
            };
            var user = _context.Users.FirstOrDefault(u => u.Id == 2);

            // Act
            var createdComment = _sut.Create(user, commentToCreate);

            var expected = _sut.GetById(user, 3);

            // Assert
            Assert.AreEqual(expected, createdComment);
        }


        [TestMethod]
        public void CreateComment__ShouldSaveToDb_WhenValid()
        {
            // Arrange
            var commentToCreate = new CreateCommentDto
            {
                Content = "I don't enjoy going to Space!",
                PostId = 4
            };
            var user = _context.Users.FirstOrDefault(u => u.Id == 2);

            // Act
            var createdPost = _sut.Create(user, commentToCreate);

            // Assert
            Assert.AreEqual(_context.Comments.Count(), 3);
        }


        //        ---------------------------------------------------------------------------------     Create In Memory Db and Seed data
        private static ForumDbContext GetInMemoryContext()
        {
            var builder = new DbContextOptionsBuilder<ForumDbContext>()
                                    .UseInMemoryDatabase("UserTestDbContext")
                                    .Options;

            return new ForumDbContext(builder);
        }

        private void SeedDatabase()
        {
            var users = new List<User>
            {
                new User
                {
                    FirstName = "Gosho",
                    LastName = "Da best",
                    Email = "gosho@gosho.com",
                    Username = "gosho123",
                    Password = "gosho123",
                    IsAdmin = true,
                    PhoneNumber = "0884343434"
                },
                new User
                {
                    FirstName = "Pesho",
                    LastName = "Not da best",
                    Email = "pesho@gosho.com",
                    Username = "pesho143",
                    Password = "pesho143"
                },
                new User
                {
                    FirstName = "Ivan",
                    LastName = "Kamenov",
                    Email = "ivan@gosho.com",
                    Username = "ivan153",
                    Password = "ivan153",
                    IsBlocked = true
                },
                new User
                {
                    FirstName = "Kosmos",
                    LastName = "Vselenov",
                    Email = "kosmos@gosho.com",
                    Username = "kosmos123",
                    Password = "kosmos123"
                }
            };

            var posts = new List<Post>
            {
                new Post
                {
                    Title = "I really don't like tests, but...",
                    Content = "When something needs to be done, it needs to be done!Pls don't blame...",
                    User = users[1],
                    UserId = users[1].Id
                },
                new Post
                {
                    Title = "Sometimes tests are nice because...",
                    Content = "when you write them, you spend a lot of time and u didn't realize it!",
                    User = users[2],
                    UserId = users[2].Id
                },
                new Post
                {
                    Title = "But now is becoming a bit annoying...",
                    Content = "Try this, try that, and in the end noone will ever read...",
                    User = users[2],
                    UserId = users[2].Id
                },
                new Post
                {
                    Title = "When I'm in 'Space' everything is so quiet",
                    Content = "Love to go to space. No noise, no traffic, no human, no nothing..Only peace!",
                    User = users[3],
                    UserId = users[3].Id
                },
                new Post
                {
                    Title = "I am Gosho and my name means everything!",
                    Content = "When I say 'Gosho' you say 'God'..Gosho...? Can't hear you!",
                    User = users[0],
                    UserId = users[0].Id
                }
            };

            var comments = new List<Comment>
            {
                new Comment
                {
                    Content = "Gosho had a nameday yesterday!",
                    PostId = posts[4].Id,
                    Post = posts[4],
                    UserId = users[1].Id,
                    User = users[1]
                },

                new Comment
                {
                    Content = "I also don't enjoy writing tests!",
                    PostId = posts[2].Id,
                    Post = posts[2],
                    UserId = users[2].Id,
                    User = users[2]
                }
            };

            _context.Users.AddRange(users);
            _context.Posts.AddRange(posts);
            _context.Comments.AddRange(comments);
            _context.SaveChanges();
        }
    }
}
