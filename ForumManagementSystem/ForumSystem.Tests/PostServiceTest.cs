﻿using ForumSystem.Data;
using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Mappers;
using ForumSystem.Services;
using ForumSystem.Services.Exceptions;
using ForumSystem.Services.Posts;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Tests
{
    [TestClass]
    public class PostServiceTest
    {
        private readonly ForumDbContext _context;
        private readonly PostService _sut;
        private readonly Mock<PostMapper> _postMapper = new();

        public PostServiceTest()
        {
            _context = GetInMemoryContext();
            _sut = new PostService(_context, _postMapper.Object);

            SeedDatabase();
        }

        //        ---------------------------------------------------------------------------------     Clean context after every test
        [TestCleanup]
        public void CleanUp()
        {
            _context.Database.EnsureDeleted();
        }



        //        ---------------------------------------------------------------------------------     Tests for GetAll
        [TestMethod]
        public void GetAllPosts_ShouldReturnCount()
        {
            // Arrange, Act
            var postsCount = _sut.GetAll().Count;

            // Assert
            Assert.AreEqual(postsCount, _context.Posts.Count());
        }


        [TestMethod]
        public void GetAllPosts_ShouldReturnAll()
        {
            // Arrange, Act
            var posts = _sut.GetAll();
            var result = _context.Posts.ToList();

            // Assert
            CollectionAssert.AreEqual(posts, result);
        }



        //        ---------------------------------------------------------------------------------     Tests for GetById
        [TestMethod]
        [ExpectedException(typeof(PostNotFoundException), "Post with id: 10 doesn't exist!")]
        public void GetPostById_ShouldThrow_WhenPostNotFound()
        {
            // Arrange, Act, Assert
            _sut.GetById(10);
        }


        [TestMethod]
        public void GetPostById_ShouldReturn_WhenValidId()
        {
            // Arrange, Act
            var post = _sut.GetById(1);
            var expected = _context.Posts.First();

            // Assert
            Assert.AreEqual(expected, post);
        }



        //        ---------------------------------------------------------------------------------     Tests for GetByTitle
        [TestMethod]
        [ExpectedException(typeof(PostNotFoundException), "Post with title: 'prekaleno mnogo si hapnah' doesn't exist!")]
        public void GetPostByTitle_ShouldThrow_WhenPostNotFound()
        {
            // Arrange, Act, Assert
            _sut.GetByTitle("prekaleno mnogo si hapnah");
        }


        [TestMethod]
        public void GetPostByTitle_ShouldReturn_WhenValidTitle()
        {
            // Arrange, Act
            var post = _sut.GetByTitle("When I'm in 'Space' everything is so quiet");
            var expected = _context.Posts.FirstOrDefault(p => p.Title == "When I'm in 'Space' everything is so quiet");

            // Assert
            Assert.AreEqual(expected, post);
        }



        //        ---------------------------------------------------------------------------------     Tests for Get with query params





        //        ---------------------------------------------------------------------------------     Tests for Create
        [TestMethod]
        [ExpectedException(typeof(AlreadyExistsException))]
        public void CreatePost_ShouldThrow_WhenAlreadyExists()
        {
            // Arrange
            var postToCreate = new PostDto
            {
                Title = "I really don't like tests, but...",
                Content = "When something needs to be done, it needs to be done!Pls don't blame..."
            };
            var user = _context.Users.First();

            // Act, Assert
            _sut.Create(user, postToCreate);
        }


        [TestMethod]
        [ExpectedException(typeof(UserAlreadyBlockedUnblocked), "User is blocked!")]
        public void CreatePost_ShouldThrow_WhenUserIsBlocked()
        {
            // Arrange
            var user = _context.Users.FirstOrDefault(u => u.Id == 3);

            // Act, Assert
            _sut.Create(user, new PostDto());
        }


        [TestMethod]
        public void CreatePost_ShouldReturnProperDto_WhenValid()
        {
            // Arrange
            var postToCreate = new PostDto
            {
                Title = "Gosho mi e priqtel oshte ot gimnaziqta",
                Content = "Gosho nqma po-dobur priqtel ot men. Az sum Pesho i sum nai-dobriqt priqtel!"
            };
            var user = _context.Users.FirstOrDefault(u => u.Id == 2);

            // Act
            var createdPost = _sut.Create(user, postToCreate);

            var expected = _context.Posts.FirstOrDefault(p => p.Title == "Gosho mi e priqtel oshte ot gimnaziqta");

            // Assert
            Assert.AreEqual(expected, createdPost);
        }


        [TestMethod]
        public void CreatePost__ShouldSaveToDb_WhenValid()
        {
            // Arrange
            var postToCreate = new PostDto
            {
                Title = "Gosho mi e priqtel oshte ot gimnaziqta",
                Content = "Gosho nqma po-dobur priqtel ot men. Az sum Pesho i sum nai-dobriqt priqtel!"
            };
            var user = _context.Users.FirstOrDefault(u => u.Id == 2);

            // Act
            var createdPost = _sut.Create(user, postToCreate);

            // Assert
            Assert.AreEqual(_context.Posts.Count(), 6);
        }


        //        ---------------------------------------------------------------------------------     Create In Memory Db and Seed data
        private static ForumDbContext GetInMemoryContext()
        {
            var builder = new DbContextOptionsBuilder<ForumDbContext>()
                                    .UseInMemoryDatabase("UserTestDbContext")
                                    .Options;

            return new ForumDbContext(builder);
        }

        private void SeedDatabase()
        {
            var users = new List<User>
            {
                new User
                {
                    FirstName = "Gosho",
                    LastName = "Da best",
                    Email = "gosho@gosho.com",
                    Username = "gosho123",
                    Password = "gosho123",
                    IsAdmin = true,
                    PhoneNumber = "0884343434"
                },
                new User
                {
                    FirstName = "Pesho",
                    LastName = "Not da best",
                    Email = "pesho@gosho.com",
                    Username = "pesho143",
                    Password = "pesho143"
                },
                new User
                {
                    FirstName = "Ivan",
                    LastName = "Kamenov",
                    Email = "ivan@gosho.com",
                    Username = "ivan153",
                    Password = "ivan153",
                    IsBlocked = true
                },
                new User
                {
                    FirstName = "Kosmos",
                    LastName = "Vselenov",
                    Email = "kosmos@gosho.com",
                    Username = "kosmos123",
                    Password = "kosmos123"
                }
            };

            var posts = new List<Post>
            {
                new Post
                {
                    Title = "I really don't like tests, but...",
                    Content = "When something needs to be done, it needs to be done!Pls don't blame...",
                    User = users[1],
                    UserId = users[1].Id
                },
                new Post
                {
                    Title = "Sometimes tests are nice because...",
                    Content = "when you write them, you spend a lot of time and u didn't realize it!",
                    User = users[2],
                    UserId = users[2].Id
                },
                new Post
                {
                    Title = "But now is becoming a bit annoying...",
                    Content = "Try this, try that, and in the end noone will ever read...",
                    User = users[2],
                    UserId = users[2].Id
                },
                new Post
                {
                    Title = "When I'm in 'Space' everything is so quiet",
                    Content = "Love to go to space. No noise, no traffic, no human, no nothing..Only peace!",
                    User = users[3],
                    UserId = users[3].Id
                },
                new Post
                {
                    Title = "I am Gosho and my name means everything!",
                    Content = "When I say 'Gosho' you say 'God'..Gosho...? Can't hear you!",
                    User = users[0],
                    UserId = users[0].Id
                }
            };

            _context.Users.AddRange(users);
            _context.Posts.AddRange(posts);
            _context.SaveChanges();
        }
    }
}
