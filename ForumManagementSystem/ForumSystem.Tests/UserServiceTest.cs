using ForumSystem.Data;
using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Mappers;
using ForumSystem.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System;
using ForumSystem.Services.Exceptions;

namespace ForumSystem.Tests
{
    [TestClass]
    public class UserServiceTest
    {
        private readonly ForumDbContext _context;
        private readonly UserService _sut;
        private readonly Mock<UserMapper> _userMapper = new();

        public UserServiceTest()
        {
            _context = GetInMemoryContext();
            _sut = new UserService(_context, _userMapper.Object);
        }

        //        ---------------------------------------------------------------------------------     Clean context after every test
        [TestCleanup]
        public void CleanUp()
        {
            _context.Database.EnsureDeleted();
        }



        //        ---------------------------------------------------------------------------------     Tests for GetAll
        [TestMethod]
        public void GetAllUsers_ShouldReturnCount()
        {
            //Arrange, Act
            SeedDatabase();

            var usersCount = _sut.GetAll().Count;

            //Assert
            Assert.AreEqual(usersCount, _context.Users.Count());
        }


        [TestMethod]
        public void GetAllUsers_ShouldReturnAll()
        {
            // Arrange
            SeedDatabase();

            // Act
            var users = _sut.GetAll();
            var expected = _context.Users.ToList();

            // Assert
            CollectionAssert.AreEqual(users, expected);
        }



        //        ---------------------------------------------------------------------------------     Tests for GetById
        [TestMethod]
        public void GetUserById_ShouldReturn_WhenValidId()
        {
            // Arrange
            SeedDatabase();

            // Act
            var user = _sut.GetUserById(1);

            var expected = _context.Users.First();
            // Assert
            Assert.AreEqual(user, expected);
        }


        [TestMethod]
        [ExpectedException(typeof(UserNotFoundException), "User not found!")]
        public void GetUserById_ShouldThrow_WhenNotFound()
        {
            // Arrange, Act, Assert
            _sut.GetUserById(6);
        }



        //        ---------------------------------------------------------------------------------     Tests for Get with querry params
        [TestMethod]
        public void GetSingleUserByUsername_Should()
        {
            //Arrange
            SeedDatabase();

            var queryWithUsername = new UserQuerryParameters { Username = "gOSho123" };

            //Act
            var user = _sut.Get(queryWithUsername);

            //Assert
            Assert.AreEqual(queryWithUsername.Username.ToLowerInvariant(), user[0].Username.ToLowerInvariant());
        }


        [TestMethod]
        public void GetSingleUserByEmail_Should()
        {
            // Arrange
            SeedDatabase();

            var queryWithEmail = new UserQuerryParameters { Email = "pesHo@goshO.com" };

            //Act
            var user = _sut.Get(queryWithEmail);

            // Assert
            Assert.AreEqual(queryWithEmail.Email.ToLowerInvariant(), user[0].Email.ToLowerInvariant());
        }


        [TestMethod]
        public void GetAllUsersByFirstName_Should()
        {
            // Arrange, Act
            SeedDatabase();

            var queryWithFirstName = new UserQuerryParameters { FirstName = "o" };

            //Act
            var users = _sut.Get(queryWithFirstName);

            var result = _context.Users
                .Where(u => u.FirstName.Contains(queryWithFirstName.FirstName,
                                                   StringComparison.InvariantCultureIgnoreCase)).ToList();


            // Assert
            CollectionAssert.AreEqual(users, result);
        }


        [TestMethod]
        public void GetAllUsersByUsername_Should()
        {
            // Arrange
            SeedDatabase();

            var queryWithUsername = new UserQuerryParameters { Username = "123" };

            // Act
            var users = _sut.Get(queryWithUsername);

            var result = _context.Users
                    .Where(u => u.Username.Contains(queryWithUsername.Username,
                                                        StringComparison.InvariantCultureIgnoreCase)).ToList();

            // Assert
            CollectionAssert.AreEqual(users, result);
        }


        [TestMethod]
        public void GetAllUsersByEmail_Should()
        {
            // Arrange
            SeedDatabase();

            var queryWithEmail = new UserQuerryParameters { Email = "gOsho.com" };

            //Act
            var users = _sut.Get(queryWithEmail);

            var result = _context.Users
                .Where(u => u.Email.Contains(queryWithEmail.Email,
                                                   StringComparison.InvariantCultureIgnoreCase)).ToList();


            // Assert
            CollectionAssert.AreEqual(users, result);
        }


        [TestMethod]
        public void GetWithDifferentParameters_Should()
        {
            // Arrange
            SeedDatabase();

            var querryWithDiffParams = new UserQuerryParameters
            {
                FirstName = "s",
                Email = "mo",
                Username = "12"
            };

            //Act
            var users = _sut.Get(querryWithDiffParams);

            var result = _context.Users
                .Where(u => u.FirstName.Contains(querryWithDiffParams.FirstName) &&
                             u.Email.Contains(querryWithDiffParams.Email) &&
                               u.Username.Contains(querryWithDiffParams.Username)).ToList();


            // Assert
            CollectionAssert.AreEqual(users, result);
        }


        [TestMethod]
        [ExpectedException(typeof(UserNotFoundException), "User not found!")]
        public void Get_ShouldThrow_WhenNotFound()
        {
            // Arrange
            SeedDatabase();

            var queryParams = new UserQuerryParameters { FirstName = "ChUbaka" };

            //Act, Assert
            _sut.Get(queryParams);
        }



        //        ---------------------------------------------------------------------------------     Tests for Create
        [TestMethod]
        public void CreateUser_ShouldReturnProperDto_WhenValid()
        {
            // Arrange
            var newUserToCreate = new UserDto
            {
                Username = "osman463",
                Password = "osman463",
                Email = "osman@gosho.com",
                FirstName = "Osman",
                LastName = "Ismail"
            };

            var resultDto = new UserResponseDto
            {
                FirstName = "Osman",
                LastName = "Ismail",
                Email = "osman@gosho.com",
                Username = "osman463"
            };

            // Act
            var createdUser = _sut.Create(newUserToCreate);


            // Assert
            Assert.AreEqual(createdUser, resultDto);
        }


        [TestMethod]
        public void CreateUser_ShouldSaveToDb_WhenValid()
        {
            // Arrange
            var newUserToCreate = new UserDto
            {
                Username = "likav325",
                Password = "likav325",
                Email = "lilav@gosho.com",
                FirstName = "Lilav",
                LastName = "Rukav"
            };

            // Act
            var createdUser = _sut.Create(newUserToCreate);

            var result = _context.Users.Single();

            // Assert
            Assert.AreEqual(result.Username, createdUser.Username);

        }


        [TestMethod]
        [ExpectedException(typeof(AlreadyExistsException), "Email already exists!")]
        public void CreateUser_ShouldThrow_WhenEmailExist()
        {
            // Arrange
            SeedDatabase();

            var newUserToCreate = new UserDto
            {
                Username = "kerkenez45",
                Password = "kerkenez45",
                Email = "kosmos@gosho.com",
                FirstName = "Kerkenez",
                LastName = "Orangutan"
            };

            // Act, Assert
            _sut.Create(newUserToCreate);
        }


        [TestMethod]
        [ExpectedException(typeof(AlreadyExistsException), "Username already exists!")]
        public void CreateUser_ShouldThrow_WhenUsernameExist()
        {
            // Arrange
            SeedDatabase();

            var newUserToCreate = new UserDto
            {
                Username = "ivan153",
                Password = "ivan153",
                Email = "neIvan@gosho.com",
                FirstName = "Ivan",
                LastName = "Stamenov"
            };

            // Act, Assert
            _sut.Create(newUserToCreate);
        }



        //        ---------------------------------------------------------------------------------     Tests for Update
        [TestMethod]
        public void UpdateUser_ShouldReturnProperDto()
        {
            // Arrange
            SeedDatabase();

            var user = _sut.GetUserById(1);
            var updateParams = new UpdateUserDto
            {
                FirstName = "GoshoVechePesho",
                LastName = "Veche ne da best",
                Email = "goshovechepesho@gosho.com",
                Password = "gosho321"
            };

            var resultDto = new UserResponseDto
            {
                FirstName = "GoshoVechePesho",
                LastName = "Veche ne da best",
                Email = "goshovechepesho@gosho.com",
                Username = "gosho123"
            };

            // Act
            var updateUser = _sut.Update(user, updateParams);


            // Assert
            Assert.AreEqual(updateUser, resultDto);
        }


        [TestMethod]
        public void UpdateUser_ShouldReturnProperDto_WhenOneParameterChange()
        {
            // Arrange
            SeedDatabase();

            var user = _sut.GetUserById(1);
            var updateParams = new UpdateUserDto { FirstName = "GoshoVechePesho" };

            var resultDto = new UserResponseDto
            {
                FirstName = "GoshoVechePesho",
                LastName = "Da best",
                Email = "gosho@gosho.com",
                Username = "gosho123"
            };

            // Act
            var updateUser = _sut.Update(user, updateParams);


            // Assert
            Assert.AreEqual(updateUser, resultDto);
        }


        [TestMethod]
        [ExpectedException(typeof(AlreadyExistsException), "Email already exists!")]
        public void UpdateUser_ShouldThrow_WhenTryToUpdateExistingEmail()
        {
            // Arrange
            SeedDatabase();

            var user = _sut.GetUserById(1);
            var updateParams = new UpdateUserDto { Email = "pesho@gosho.com" };

            // Act, Assert
            _sut.Update(user, updateParams);
        }



        //        ---------------------------------------------------------------------------------     Tests for Delete
        [TestMethod]
        [ExpectedException(typeof(UserNotFoundException), "User not found!")]
        public void DeleteUser_ShouldThrow_WhenUserNotFound()
        {
            // Arrange, Act, Assert
            _sut.Delete(10);
        }


        [TestMethod]
        public void DeleteUser_ShouldDelete_WhenUserFound()
        {
            // Arrange
            SeedDatabase();
            var currentLogUser = _sut.GetUserById(_context.Users.Count());

            var expected = _sut.GetUserById(_context.Users.Count() - 1);

            // Act
            _sut.Delete(3);

            var lastUserInContext = _context.Users.Last();

            // Assert
            Assert.AreEqual(lastUserInContext, expected);
        }



        //        ---------------------------------------------------------------------------------     Tests for Authenticate
        [TestMethod]
        [ExpectedException(typeof(UserNotFoundException), "User not found!")]
        public void Authenticate_ShouldThrow_WhenCredentialsDoesntMatch()
        {
            // Arrange
            SeedDatabase();

            // Act, ASsert
            _sut.Authenticate("GarikPiotr", "AvadaKedavra");
        }


        [TestMethod]
        public void Authenticate_ShouldReturnUser_WhenValidCredentials()
        {
            // Arrange
            SeedDatabase();
            var expectedUser = _sut.GetUserById(1);

            // Act
            var authUser = _sut.Authenticate("gosho123", "gosho123");

            // Assert
            Assert.AreEqual(expectedUser, authUser);
        }



        //        ---------------------------------------------------------------------------------     Tests for Block, Unblock, Change to Admin

        [TestMethod]
        [ExpectedException(typeof(UserNotFoundException), "User not found!")]
        public void ChangeRoleOrBlock_ShouldThrow_WhenUserNotFound()
        {
            // Arrange, Act, Assert
            _sut.ChangeRoleOrBlock("ivanka9", "block", "");
        }


        [TestMethod]
        [ExpectedException(typeof(InvalidInputException), "Invalid input for action / 'block', 'unblock' or 'admin'!")]
        public void ChangeRoleOrBlock_ShouldThrow_WhenActionNotValid()
        {
            // Arrange
            SeedDatabase();

            // Act, Assert
            _sut.ChangeRoleOrBlock("gosho123", "da hapne", "");
        }


        [TestMethod]
        [ExpectedException(typeof(UserAlreadyBlockedUnblocked))]
        public void BlockUser_ShouldThrow_WhenAlreadyBlocked()
        {
            // Arrange
            SeedDatabase();

            // Act, Assert
            _sut.ChangeRoleOrBlock("ivan153", "block", "");
        }


        [TestMethod]
        [ExpectedException(typeof(UserAlreadyBlockedUnblocked))]
        public void UnblockUser_ShouldThrow_WhenAlreadyUnblocked()
        {
            // Arrange
            SeedDatabase();

            // Act, Assert
            _sut.ChangeRoleOrBlock("pesho143", "unblock", "");
        }


        [TestMethod]
        [ExpectedException(typeof(UserIsAdminException))]
        public void BlockUser_ShouldThrow_WhenUserIsAdmin()
        {
            // Arrange
            SeedDatabase();

            // Act, Assert
            _sut.ChangeRoleOrBlock("gosho123", "block", "");
        }


        [TestMethod]
        [ExpectedException(typeof(UserIsAdminException))]
        public void UnblockUser_ShouldThrow_WhenUserIsAdmin()
        {
            // Arrange
            SeedDatabase();

            // Act, Assert
            _sut.ChangeRoleOrBlock("gosho123", "unblock", "");
        }


        [TestMethod]
        [ExpectedException(typeof(UserIsAdminException))]
        public void ChangeRoleToAdmin_ShouldThrow_WhenUserAlreadyAdmin()
        {
            // Arrange
            SeedDatabase();

            // Act, Assert
            _sut.ChangeRoleOrBlock("gosho123", "admin", "0885454545");
        }


        [TestMethod]
        public void BlockUser_ShouldBlock_WhenValid()
        {
            // Arrange
            SeedDatabase();

            // Act
            _sut.ChangeRoleOrBlock("pesho143", "block", "");
            var user = _sut.GetUserById(2);

            // Assert
            Assert.IsTrue(user.IsBlocked);
        }


        [TestMethod]
        public void UnblockUser_ShouldUnblock_WhenValid()
        {
            // Arrange
            SeedDatabase();

            // Act
            _sut.ChangeRoleOrBlock("ivan153", "unblock", "");
            var user = _sut.GetUserById(3);

            // Assert
            Assert.IsFalse(user.IsBlocked);
        }


        [TestMethod]
        public void ChangeRoleToAdmin_ShouldChange_WhenValid()
        {
            // Arrange
            SeedDatabase();

            // Act
            _sut.ChangeRoleOrBlock("pesho143", "admin", "0881212121");
            var user = _sut.GetUserById(2);

            // Assert
            Assert.IsTrue(user.IsAdmin);
        }



        //        ---------------------------------------------------------------------------------     Tests for Get count of registered users
        [TestMethod]
        public void GetRegisteredUsers_ShouldReturnCount()
        {
            // Arrange
            SeedDatabase();

            // Act
            var countUsers = _sut.GetRegisteredUsersCount();
            var expected = _context.Users.Count();
            // Assert
            Assert.AreEqual(expected, countUsers);
        }



        //        ---------------------------------------------------------------------------------     Create In Memory Db and Seed data
        private static ForumDbContext GetInMemoryContext()
        {
            var builder = new DbContextOptionsBuilder<ForumDbContext>()
                                    .UseInMemoryDatabase("UserTestDbContext")
                                    .Options;

            return new ForumDbContext(builder);
        }

        private void SeedDatabase()
        {
            var users = new List<User>
            {
                new User
                {
                    FirstName = "Gosho",
                    LastName = "Da best",
                    Email = "gosho@gosho.com",
                    Username = "gosho123",
                    Password = "gosho123",
                    IsAdmin = true,
                    PhoneNumber = "0884343434"
                },
                new User
                {
                    FirstName = "Pesho",
                    LastName = "Not da best",
                    Email = "pesho@gosho.com",
                    Username = "pesho143",
                    Password = "pesho143"
                },
                new User
                {
                    FirstName = "Ivan",
                    LastName = "Kamenov",
                    Email = "ivan@gosho.com",
                    Username = "ivan153",
                    Password = "ivan153",
                    IsBlocked = true
                },
                new User
                {
                    FirstName = "Kosmos",
                    LastName = "Vselenov",
                    Email = "kosmos@gosho.com",
                    Username = "kosmos123",
                    Password = "kosmos123"
                }
            };

            _context.Users.AddRange(users);
            _context.SaveChanges();
        }
    }
}
