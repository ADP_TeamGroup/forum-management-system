﻿using ForumSystem.Data;
using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Dto_s;
using ForumSystem.Data.Models.Enums;
using ForumSystem.Data.Models.ViewModels;
using ForumSystem.Services.Exceptions;
using ForumSystem.Services.Posts;
using ForumSystem.Web.Controllers.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace ForumSystem.Web.Controllers
{

    public class PostsController : Controller
    {
        private readonly ForumDbContext _forumDb;
        private readonly IPostService _postService;
        private readonly AuthenticationHelper _authenticationHelper;

        public PostsController(ForumDbContext forumDb, IPostService postService, AuthenticationHelper authenticationHelper)
        {
            _forumDb = forumDb;
            _postService = postService;
            _authenticationHelper = authenticationHelper;
        }
        public IActionResult Index(string option, string input)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return RedirectToAction("Login", "Home");
            }
            var postViewIndexModel = new PostsIndexModel();
            var username = this.HttpContext.Session.GetString("CurrentUser");
            var currentUser = _authenticationHelper.TryGetUser(username);
            this.ViewData["IsBlocked"] = (bool)currentUser.IsBlocked;
            this.ViewData["IsAdmin"] = (bool)currentUser.IsAdmin;
            try
            {
                postViewIndexModel.Posts = _postService.GetFilteredPosts(option, input);
                postViewIndexModel.PostTagsContent = _postService.GetPostTagsContent(postViewIndexModel.Posts);
                return View(postViewIndexModel);
            }
            catch (PostNotFoundException e)
            {
                TempData["Posts Not Found"] = e.Message;
                postViewIndexModel.Posts = _postService.GetFilteredPosts(string.Empty, string.Empty);
                postViewIndexModel.PostTagsContent = _postService.GetPostTagsContent(postViewIndexModel.Posts);

                return View(postViewIndexModel);
            }

        }
        [HttpPost]
        public IActionResult Index(string button)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return RedirectToAction("Login", "Home");
            }
            var username = this.HttpContext.Session.GetString("CurrentUser");
            var currentUser = _authenticationHelper.TryGetUser(username);
            if (int.TryParse(button, out int postId))
            {
                _postService.Delete( postId);
            }
            else
            {
                var postResponse = _postService.UpdateLikes(currentUser, button);
                TempData["Successfully Voted"] = $"{postResponse.Username} reacted to {postResponse.Title}";
            }
            return RedirectToAction("Index", "Posts");
        }


        public IActionResult Create()
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return RedirectToAction("Login", "Home");
            }
            var username = this.HttpContext.Session.GetString("CurrentUser");
            var currentUser = _authenticationHelper.TryGetUser(username);
            if (currentUser.IsBlocked)
            {
                return RedirectToAction("Index", "Posts");
            }
            return View();
        }
        [HttpPost]
        public IActionResult Create(PostDto postDto)
        {

            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return RedirectToAction("Login", "Home");
            }
            var username = this.HttpContext.Session.GetString("CurrentUser");
            var currentUser = _authenticationHelper.TryGetUser(username);
            if (currentUser.IsBlocked)
            {
                return RedirectToAction("Login", "Home");
            }
            var postResponse = _postService.Create(currentUser, postDto);
            TempData["Successfully Created"] = $"{postResponse.Username} created a post: {postResponse.Title}";
            return RedirectToAction("Create", "Posts");
        }
      

        public IActionResult Personal()
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return RedirectToAction("Login", "Home");
            }
            var username = this.HttpContext.Session.GetString("CurrentUser");
            var user = _authenticationHelper.TryGetUser(username);

            var postViewIndexModel = new PostsIndexModel();
            postViewIndexModel.Posts = _forumDb.Posts
                .Include(post => post.User)
                .Include(post => post.Tags)
                    .ThenInclude(tag => tag.Tag)
                    .Where(p => p.User.Equals(user));

            postViewIndexModel.PostTagsContent = _postService.GetPostTagsContent(postViewIndexModel.Posts);

            return View(postViewIndexModel);
        }
        public IActionResult Edit(int postId)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return RedirectToAction("Login", "Home");
            }
            try
            {

                var postViewIndexModel = new PostEditModel();
                postViewIndexModel.Post = _postService.GetById(postId);
                if (this.HttpContext.Session.GetString("CurrentUser") != postViewIndexModel.Post.User.Username)
                {
                    throw new NotAuthenticatedException("You can't edit posts of other users!");
                }
                postViewIndexModel.Title = postViewIndexModel.Post.Title;
                postViewIndexModel.Content = postViewIndexModel.Post.Content;

                return View(postViewIndexModel);
            }
            catch (NotAuthenticatedException e)
            {
                TempData["Unauthorized"] = e.Message;
                return RedirectToAction("Index", "Posts");
            }
        }
        [HttpPost]
        public IActionResult Edit(PostEditModel postEditModel, string btnDelete)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return RedirectToAction("Login", "Home");
            }
            postEditModel.Id = postEditModel.Post.Id;
            postEditModel.Post = _postService.GetById(postEditModel.Id);

            if (!string.IsNullOrEmpty(btnDelete))
            {
                _postService.Delete(this.HttpContext.Session.GetString("CurrentUser"), int.Parse(btnDelete));
                this.TempData["Successfully Deleted"] = $"{postEditModel.Post.User.Username} updated post {postEditModel.Post.Title}";
                return RedirectToAction("Personal", "Posts");
            }
            var updatedPost = _postService.EditPost(postEditModel);
            this.TempData["Successfully Edited"] = $"{updatedPost.Username} updated post {updatedPost.Content}";
            return RedirectToAction("Personal", "Posts");
        }


    }
}
