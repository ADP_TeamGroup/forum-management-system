﻿using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Mappers;
using ForumSystem.Services.Exceptions;
using ForumSystem.Services.Interfaces;
using ForumSystem.Web.Controllers.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace ForumSystem.Web.Controllers.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class CommentsControllerApi : ControllerBase
    {
        private readonly ICommentService _commentService;
        private readonly AuthenticationHelper _authenticationHelper;
        private readonly CommentMapper _commentMapper;

        public CommentsControllerApi(ICommentService commentService, AuthenticationHelper authenticationHelper, CommentMapper mapper)
        {
            _commentService = commentService;
            _authenticationHelper = authenticationHelper;
            _commentMapper = mapper;
        }
        

        [HttpGet("{id}")]
        public IActionResult GetCommentById([FromHeader] string username, [FromHeader] string password, int id)
        {
            try
            {
                var currentUser = _authenticationHelper.TryGetUser(username, password);
                var comment = this._commentService.GetById(currentUser, id);
                return this.StatusCode(StatusCodes.Status200OK, comment);
            }
            catch (CommentNotFoundException notFound)
            {
                return BadRequest(notFound.Message);
            }
            catch (NotAuthenticatedException notAuthenticated)
            {
                return Unauthorized(notAuthenticated.Message);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UserAlreadyBlockedUnblocked blocked)
            {
                return Unauthorized(blocked.Message);
            }
            catch (ArgumentNullException exception)
            {
                return NotFound(exception.Message);
            }
        }

        [HttpGet("")]
        public IActionResult GetAllCommentsforPost([FromHeader] string username, [FromHeader] string password, [FromQuery] CommentQueryParameters filterParameters)
        {
            try
            {
                var currentUser = this._authenticationHelper.TryGetUser(username, password);
                var comments = this._commentService.GetAllForPost(currentUser, filterParameters);
                return this.StatusCode(StatusCodes.Status200OK, comments);
            }
            catch (CommentNotFoundException notFound)
            {
                return BadRequest(notFound.Message);
            }
            catch (NotAuthenticatedException notAuthenticated)
            {
                return Unauthorized(notAuthenticated.Message);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UserAlreadyBlockedUnblocked blocked)
            {
                return Unauthorized(blocked.Message);
            }
        }


        [HttpPost]
        public IActionResult Create([FromHeader] string username
                                , [FromHeader] string password, [FromBody] CreateCommentDto commentDto)
        {
            try
            {
                var currentUser = this._authenticationHelper.TryGetUser(username, password);
                var createdComment = this._commentService.Create(currentUser, commentDto);
                return Created($"Comment was created successfuly!", createdComment);
            }
            catch (NotAuthenticatedException notAuthenticated)
            {
                return Unauthorized(notAuthenticated.Message);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UserAlreadyBlockedUnblocked blocked)
            {
                return Unauthorized(blocked.Message);
            }
            catch (EntityNotFoundException e)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id,
                            [FromHeader] string username
                          , [FromHeader] string password
                          , [FromBody] UpdateCommentDto commentDto)
        {
            try
            {
                var currentUser = this._authenticationHelper.TryGetUser(username, password);

                var updatedComment = this._commentService.Update(id, currentUser, commentDto);

                return Created($"User:{currentUser.Username} updated comment: {id} ", updatedComment);
            }
            catch (NotAuthenticatedException notAuthenticated)
            {
                return Unauthorized(notAuthenticated.Message);
            }
            catch (AlreadyExistsException alreadyExist)
            {
                return Conflict(alreadyExist.Message);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput.Message);
            }
            catch (UserAlreadyBlockedUnblocked blocked)
            {
                return Unauthorized(blocked.Message);
            }
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id,
                            [FromHeader] string username
                          , [FromHeader] string password)
        {
            try
            {
                var currentUser = this._authenticationHelper.TryGetUser(username, password);
                this._commentService.Delete(username, id);
                return Ok($"Comment : {id} was succesfully deleted!");
            }
            catch (NotAuthenticatedException notAuthenticated)
            {
                return Unauthorized(notAuthenticated.Message);
            }
            catch (UnauthorizedException unauthorized)
            {
                return Unauthorized(unauthorized.Message);
            }
            catch (UserNotFoundException notFound)
            {
                return NotFound(notFound.Message);
            }
            catch (CommentNotFoundException commentNotFound)
            {
                return NotFound(commentNotFound.Message);
            }

        }
    }
}
