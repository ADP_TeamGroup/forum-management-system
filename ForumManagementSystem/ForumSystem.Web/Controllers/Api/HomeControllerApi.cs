﻿using ForumSystem.Data.Models;
using ForumSystem.Services;
using ForumSystem.Services.Exceptions;
using ForumSystem.Services.Posts;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ForumSystem.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HomeControllerApi : Controller
    {
        private readonly IUserService _userService;
        private readonly IPostService _postService;

        public HomeControllerApi(IUserService userService, IPostService postService)
        {
            _userService = userService;
            _postService = postService;
        }

        [HttpGet("")]
        public IActionResult HomePage()
        {
            return Json((_postService.MostCommented(), _postService.RecentlyCreated()));
        }

        [HttpPost("register")]
        public IActionResult Register([FromBody] UserDto newUser)
        {
            try
            {
                var user = _userService.Create(newUser);
                return Created($"User: {user.FirstName} {user.LastName} is created", user);
            }
            catch (AlreadyExistsException alreadyExist)
            {
                return Conflict(alreadyExist.Message);
            }
        }
    }
}
