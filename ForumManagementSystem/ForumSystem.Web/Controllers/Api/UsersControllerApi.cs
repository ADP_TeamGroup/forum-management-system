﻿using ForumSystem.Data.Models;
using ForumSystem.Services;
using ForumSystem.Services.Exceptions;
using ForumSystem.Web.Controllers.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ForumSystem.Web.Controllers.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersControllerApi : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly AuthenticationHelper _authorizationHelper;

        public UsersControllerApi(IUserService userService, AuthenticationHelper authorizationHelper)
        {
            _userService = userService;
            _authorizationHelper = authorizationHelper;
        }

        [HttpGet("")]
        public IActionResult Get([FromHeader] string username,
                                 [FromHeader] string password,
                                 [FromQuery] UserQuerryParameters userQueryParams)
        {
            try
            {
                var currentUser = _authorizationHelper.TryGetUser(username, password);
                _authorizationHelper.AdminAuthorization(currentUser);

                if (userQueryParams.IsEmpty())
                {
                    return Ok(_userService.GetAll());
                }

                return Ok(_userService.Get(userQueryParams));
            }
            catch (NotAuthenticatedException notAuthenticated)
            {
                return Unauthorized(notAuthenticated.Message);
            }
            catch (UnauthorizedException unauthorized)
            {
                return Unauthorized(unauthorized.Message);
            }
            catch (UserNotFoundException notFound)
            {
                return NotFound(notFound.Message);
            }
        }

        [HttpPut("")]
        public IActionResult UpdateUser([FromHeader] string username,
                                        [FromHeader] string password,
                                        [FromQuery] UpdateUserDto updateParams)
        {
            try
            {
                var currentUser = _authorizationHelper.TryGetUser(username, password);
                var user = _userService.Update(currentUser, updateParams);

                return Ok(user);
            }
            catch (NotAuthenticatedException notAuthenticated)
            {
                return Unauthorized(notAuthenticated.Message);
            }
            catch (AlreadyExistsException alreadyExist)
            {
                return Conflict(alreadyExist.Message);
            }
            catch (InvalidInputException invalidInput)
            {
                return BadRequest(invalidInput);
            }
        }

        [HttpPut("{usernameToChangePrivileges}")]
        public IActionResult ChangeRoleOrBlock(string usernameToChangePrivileges,
                                              [FromQuery] string action,
                                              [FromQuery] string phoneNumber,
                                              [FromHeader] string username,
                                              [FromHeader] string password)
        {
            try
            {
                var currentUser = _authorizationHelper.TryGetUser(username, password);
                _authorizationHelper.AdminAuthorization(currentUser);

                _userService.ChangeRoleOrBlock(usernameToChangePrivileges, action.ToLower(), phoneNumber);

                if (action.ToLower() == "admin")
                {
                    return Ok($"User : {usernameToChangePrivileges} is switched to Admin.");
                }
                return Ok($"User : {usernameToChangePrivileges} was succesfully {action}ed!");
            }
            catch (NotAuthenticatedException notAuthenticated)
            {
                return Unauthorized(notAuthenticated.Message);
            }
            catch (UnauthorizedException unauthorized)
            {
                return Unauthorized(unauthorized.Message);
            }
            catch (UserNotFoundException notFound)
            {
                return NotFound(notFound.Message);
            }
            catch (UserAlreadyBlockedUnblocked blockedUnblocked)
            {
                return BadRequest(blockedUnblocked.Message);
            }
            catch (UserIsAdminException isAdmin)
            {
                return BadRequest(isAdmin.Message);
            }
        }

        [HttpDelete("{usernameToDelete}")]
        public IActionResult DeleteUser(int userId,
                                        [FromHeader] string username,
                                        [FromHeader] string password)
        {
            try
            {
                var currentUser = _authorizationHelper.TryGetUser(username, password);

                _userService.Delete(userId);

                return Ok($"User with ID: {userId} was succesfully deleted!");
            }
            catch (NotAuthenticatedException notAuthenticated)
            {
                return Unauthorized(notAuthenticated.Message);
            }
            catch (UnauthorizedException unauthorized)
            {
                return Unauthorized(unauthorized.Message);
            }
            catch (UserNotFoundException notFound)
            {
                return NotFound(notFound.Message);
            }
            catch (InvalidDeleteRequestException dontHaveAccess)
            {
                return Unauthorized(dontHaveAccess.Message);
            }
        }
    }
}
