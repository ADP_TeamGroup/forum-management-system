﻿using ForumSystem.Data.Models;
using ForumSystem.Services;
using ForumSystem.Services.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace ForumSystem.Web.Controllers.Helpers
{
    public class AuthenticationHelper
    {
        private readonly IUserService _userService;


        public AuthenticationHelper(IUserService userService)
        {
            _userService = userService;
        }

        public User TryGetUser(string username, string password)
        {
            try
            {
                return _userService.Authenticate(username, password);
            }
            catch (UserNotFoundException)
            {
                throw new NotAuthenticatedException("Invalid credentials! / User does not exist");
            }

        }

        public User TryGetUser(string username)
        {
            try
            {
                return _userService.Authenticate(username);
            }
            catch (UserNotFoundException)
            {
                throw new NotAuthenticatedException("Invalid credentials! / User does not exist");
            }

        }

        public void AdminAuthorization(User checkAdmin)
        {
            if (!checkAdmin.IsAdmin)
            {
                throw new UnauthorizedException("You don't have access to this request!");
            }
        }

      
    }
}
