﻿using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Dto_s;
using ForumSystem.Data.Models.Mappers;
using ForumSystem.Services.Posts;

namespace ForumSystem.Web.Controllers.Helpers
{
    public class PostHelper
    {
        private readonly IPostService _postService;
        private readonly PostMapper _postMapper;

        public PostHelper(IPostService postService, PostMapper postMapper)
        {
            _postService = postService;
            _postMapper = postMapper;
        }

        public Post TryGetPostByTitle(string postTitle)
        {
            var post = _postService.GetByTitle(postTitle);
            return post;
        }
        public PostDto GetPostDto(UpdatePostDto updatePostDto,Post post)
        {
            var postDto = _postMapper.ConvertToPostDto(updatePostDto, post);
            return postDto;
        }

    }
}
