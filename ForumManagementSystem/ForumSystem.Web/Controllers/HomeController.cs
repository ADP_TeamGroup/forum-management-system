﻿using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Dto_s;
using ForumSystem.Services;
using ForumSystem.Services.Exceptions;
using ForumSystem.Services.Posts;
using ForumSystem.Web.Controllers.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ForumSystem.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService _userService;
        private readonly IPostService _postService;
        private readonly AuthenticationHelper _authenticationHelper;


        public HomeController(IUserService userService, IPostService postService, AuthenticationHelper authenticationHelper)
        {
            _userService = userService;
            _postService = postService;
            _authenticationHelper = authenticationHelper;
        }

        public IActionResult Index()
        {
            var recentlyCreated = _postService.RecentlyCreated();
            var mostCommentedPosts = _postService.MostCommented();

            var homePosts = new Tuple<List<PostResponseDto>, List<PostResponseDto>>(recentlyCreated, mostCommentedPosts);

            return this.View(homePosts);
        }

        public IActionResult About()
        {
            return this.View();
        }

        public IActionResult Login()
        {
            if (this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            return this.View();
        }

        [HttpPost]
        public IActionResult Login(UserLogin userLogin)
        {
            try
            {
                var userToAuthenticate = _authenticationHelper.TryGetUser(userLogin.Username, userLogin.Password);
                this.HttpContext.Session.SetString("CurrentUser", userToAuthenticate.Username);

                if (userToAuthenticate.IsAdmin)
                {
                    this.HttpContext.Session.SetString("Role", "Admin");
                }
                else
                {
                    this.HttpContext.Session.SetString("Role", "User");
                }

                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (NotAuthenticatedException notAuth)
            {
                TempData["WrongCredentials"] = notAuth.Message;

                return RedirectToAction(actionName: "Login", controllerName: "Home");
            }
        }

        public IActionResult Logout()
        {
            this.HttpContext.Session.Clear();

            return this.RedirectToAction(actionName: "Index", controllerName: "Home");
        }

        public IActionResult Register()
        {
            if (this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            return this.View();
        }

        [HttpPost]
        public IActionResult Register(UserDto newUser)
        {
            try
            {
                var user = _userService.Create(newUser);
                this.HttpContext.Session.SetString("CurrentUser", user.Username);
                this.HttpContext.Session.SetString("Role", "User");

                return this.RedirectToAction(actionName: "Index", controllerName: "Home");
            }
            catch (AlreadyExistsException alreadyExist)
            {
                TempData["AlreadyExist"] = alreadyExist.Message;

                return this.View();
            }
        }
    }
}
