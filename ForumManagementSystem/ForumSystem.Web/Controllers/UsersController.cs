﻿using ForumSystem.Data.Models;
using ForumSystem.Services;
using ForumSystem.Services.Exceptions;
using ForumSystem.Web.Controllers.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace ForumSystem.Web.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        public IActionResult AllUsers(string option, string input)
        {
            if (this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                try
                {
                    var userQueryParams = new UserQuerryParameters();

                    switch (option)
                    {
                        case "Username":
                            userQueryParams.Username = input;
                            break;
                        case "FirstName":
                            userQueryParams.FirstName = input;
                            break;
                        case "LastName":
                            userQueryParams.LastName = input;
                            break;
                        case "Email":
                            userQueryParams.Email = input;
                            break;
                    }

                    if (userQueryParams.IsEmpty())
                    {
                        return this.View(_userService.GetAll());
                    }

                    return this.View(_userService.Get(userQueryParams));
                }
                catch (UserNotFoundException notFound)
                {
                    TempData["NotFound"] = notFound.Message;
                    return this.View();
                }

            }

            return RedirectToAction(actionName: "Index", controllerName: "Home");
        }

        public IActionResult Details()
        {
            var currentUser = this.HttpContext.Session.GetString("CurrentUser");
            var user = _userService.GetByUsername(currentUser);

            return this.View(user);
        }

        public IActionResult Edit(int userId)
        {
            if (this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                var user = _userService.GetUserById(userId);
                return this.View(user);
            }
            return RedirectToAction(actionName: "Index", controllerName: "Home");
        }

        [HttpPost]
        public IActionResult Edit(int userId, UpdateUserDto updateDetails)
        {
            try
            {
                var user = _userService.GetUserById(userId);
                var updatedUser = _userService.Update(user, updateDetails);
                if (this.HttpContext.Session.GetString("CurrentUser") == user.Username)
                {
                    return RedirectToAction(actionName: "Details", controllerName: "Users");
                }

                return RedirectToAction(actionName: "AllUsers", controllerName: "Users");
            }
            catch (AlreadyExistsException alreadyExists)
            {
                TempData["AlreadyExist"] = alreadyExists.Message;

                return this.View(userId);
            }
        }

        public IActionResult Delete(int userId)
        {
            _userService.Delete(userId);

            return RedirectToAction(actionName: "AllUsers", controllerName: "Users");
        }
    }
}
