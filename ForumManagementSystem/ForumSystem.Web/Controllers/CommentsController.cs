﻿using ForumSystem.Data;
using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Mappers;
using ForumSystem.Data.Models.ViewModels;
using ForumSystem.Services;
using ForumSystem.Services.Exceptions;
using ForumSystem.Services.Interfaces;
using ForumSystem.Services.Posts;
using ForumSystem.Web.Controllers.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ForumSystem.Web.Controllers
{
    public class CommentsController : Controller
    {
        private readonly ForumDbContext _forumDb;
        private readonly IPostService _postService;
        private readonly ICommentService _commentService;
        private readonly ForumDbContext _database;
        private readonly CommentMapper _commentMapper;
        public CommentsController(ForumDbContext forumDb, IPostService postService, ICommentService commentService, ForumDbContext database, CommentMapper commentMapper)
        {
            _forumDb = forumDb;
            _postService = postService;
            _commentService = commentService;
            _database = database;
            _commentMapper = commentMapper;
        }

        public IActionResult Create(int postId)
        {
            try
            {
                var viewModel = new CommentsModel();
                viewModel.Post = this._postService.GetById(postId);
                viewModel.PostId = postId;
                viewModel.Comments = viewModel.Post.Comments;
                return View(viewModel);
            }
            catch (EntityNotFoundException)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                this.ViewData["ErrorMessage"] = $"Post with id {postId} does not exist.";

                return this.View(viewName: "Error");
            }
        }

        [HttpPost]
        public IActionResult Create(CommentsModel viewModel)
       {
            var post = this._postService.GetById(viewModel.PostId);
            viewModel.Post = post;
            var username = HttpContext.Session.GetString("CurrentUser");
            var user = _database.Users.FirstOrDefault(u => u.Username == username);
            var comment = this._commentMapper.ConvertToModel(user, viewModel);
            this._database.Comments.Add(comment);
            this._database.SaveChanges();

            return RedirectToAction("GetComments", new { postId = post.Id });
        }

        public IActionResult Edit(int commentId)
        {
            if (!this.HttpContext.Session.Keys.Contains("CurrentUser"))
            {
                return RedirectToAction("Login", "Home");
            }
            try
            {

                var username = HttpContext.Session.GetString("CurrentUser");
                var user = _database.Users.FirstOrDefault(u => u.Username == username);
                var viewModel = new CommentsModel();

                viewModel.Comment = this._commentService.Get(user, commentId);
                viewModel.Content = viewModel.Comment.Content;
                viewModel.PostId = viewModel.Comment.Post.Id;
                viewModel.Comments = viewModel.Comment.Post.Comments;

                return View(viewModel);
            }
            catch (Exception ex)
            {
                throw new NotImplementedException();
            }
        }

        [HttpPost]
        public IActionResult Edit(CommentsModel viewModel, string Submit)
        {
            var username = HttpContext.Session.GetString("CurrentUser");
            
            viewModel.Username = username;
            viewModel.CommentId = int.Parse(Submit);
            var updatedComment = this._commentService.Update(viewModel);

            return RedirectToAction("GetComments", new { postId = updatedComment.Post.Id });
        }

        public IActionResult Delete(int commentId)
        {
            var username = HttpContext.Session.GetString("CurrentUser");
            var user = _database.Users.FirstOrDefault(u => u.Username == username);
            var comment = this._commentService.Get(user, commentId);
            this._commentService.Delete(username, commentId);

            return RedirectToAction("GetComments", new { postId = comment.Post.Id });
        }

        public IActionResult GetComments(int postId)
        {
            try
            {
                var viewModel = new CommentsModel();
                var username = HttpContext.Session.GetString("CurrentUser");
                viewModel.Post = this._postService.GetById(postId);
                viewModel.Comments = viewModel.Post.Comments;
                viewModel.PostId = postId;
                viewModel.Username = username;
                

                return View(viewModel);
            }
            catch (EntityNotFoundException)
            {
                this.Response.StatusCode = StatusCodes.Status404NotFound;
                this.ViewData["ErrorMessage"] = $"Post with id {postId} does not exist.";

                return this.View(viewName: "Error");
            }

        }
    }
}
