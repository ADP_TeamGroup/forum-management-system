﻿using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Dto_s;
using ForumSystem.Data.Models.Enums;
using ForumSystem.Data.Models.QueryParams;
using ForumSystem.Data.Models.ViewModels;
using System.Collections.Generic;

namespace ForumSystem.Services.Posts
{
    public interface IPostService
    {
        public List<PostResponseDto> GetAllPostResponseDtos();
        List<PostResponseDto> Get(PostQueryParameters queryParams);
        Post GetById(int id);
        Post GetByTitle(string title);
        PostResponseDto Create(User user, PostDto postDto);
        Dictionary<int, List<string>> GetPostTagsContent(IEnumerable<Post> posts);
        PostResponseDto Update(int postToUpdateId, UpdatePostDto postDto, User currentUser);
        PostResponseDto UpdateLikes(int id, User currentUser
                                         , PostVoteQueryParameters postVoteQuery);
        public PostResponseDto UpdateLikes(User currentUser, string reaction);
        public PostResponseDto EditPost(PostEditModel postEditModel);
        void Delete(string username, int postId, bool isAdmin);

        void Delete(string username, int postId);
        void Delete(int postId);
        public IEnumerable<Post> GetFilteredPosts(string option, string input);
        public PostResponseDto DeleteTag(int postId, User currentUser, TagQueryParameters tagQuery);

        List<PostResponseDto> RecentlyCreated();
        List<PostResponseDto> MostCommented();
    }
}
