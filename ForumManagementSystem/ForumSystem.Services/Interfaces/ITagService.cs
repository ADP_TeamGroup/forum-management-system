﻿using ForumSystem.Data.Models;

namespace ForumSystem.Services
{
    public interface ITagService
    {
        Tag Create(string tagName);
    }
}