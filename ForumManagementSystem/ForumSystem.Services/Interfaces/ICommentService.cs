﻿using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Mappers;
using ForumSystem.Data.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Services.Interfaces
{
    public interface ICommentService
    {
        ResponseCommentDto GetById(User user, int id);
        Comment Get(User user, int id);

        List<ResponseCommentDto> GetAllForPost(User user, CommentQueryParameters filterParameters);

        ResponseCommentDto Create(User user, CreateCommentDto comment);

        ResponseCommentDto Update(int id, User user, UpdateCommentDto commentDto);
        Comment Update (CommentsModel viewModel);

        Comment Delete(string username, int id);
    }
}
