﻿using ForumSystem.Data.Models;
using System.Collections.Generic;

namespace ForumSystem.Services
{
    public interface IUserService
    {
        User GetUserById(int id);
        User GetByUsername(string username);
        List<User> Get(UserQuerryParameters userParameter);
        List<User> GetAll();
        UserResponseDto Create(UserDto user);
        UserResponseDto Update(User currentUser, UpdateUserDto updateParams);
        int GetRegisteredUsersCount();
        void Delete(int userId);
        User Authenticate(string username, string password);

        User Authenticate(string username);
        void ChangeRoleOrBlock(string username, string action, string phoneNumber);
    }
}