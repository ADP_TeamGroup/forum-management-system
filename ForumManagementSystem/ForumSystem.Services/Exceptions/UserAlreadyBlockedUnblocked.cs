﻿using System;

namespace ForumSystem.Services.Exceptions
{
    public class UserAlreadyBlockedUnblocked : ApplicationException
    {
        public UserAlreadyBlockedUnblocked(string message) : base(message) { }
    }
}
