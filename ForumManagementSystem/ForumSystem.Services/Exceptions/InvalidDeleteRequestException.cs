﻿using System;

namespace ForumSystem.Services.Exceptions
{
    public class InvalidDeleteRequestException : ApplicationException
    {
        public InvalidDeleteRequestException(string message) : base(message) { }
    }
}
