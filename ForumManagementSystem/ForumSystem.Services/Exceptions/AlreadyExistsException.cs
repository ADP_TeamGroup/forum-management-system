﻿using System;

namespace ForumSystem.Services.Exceptions
{
    public class AlreadyExistsException : ApplicationException
    {
        public AlreadyExistsException(string message) : base(message) { }
    }
}
