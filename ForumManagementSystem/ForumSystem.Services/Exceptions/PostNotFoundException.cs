﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Services.Exceptions
{
    public class PostNotFoundException:ApplicationException
    {
        public PostNotFoundException(string massage) : base(massage)
        {
        }
    }
}
