﻿using System;


namespace ForumSystem.Services.Exceptions
{
    public class UserIsAdminException : ApplicationException
    {
        public UserIsAdminException(string message) : base(message) { }
    }
}
