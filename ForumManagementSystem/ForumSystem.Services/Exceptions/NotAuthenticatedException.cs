﻿using System;

namespace ForumSystem.Services.Exceptions
{
    public class NotAuthenticatedException : ApplicationException
    {
        public NotAuthenticatedException(string message) : base(message) { }

    }
}
