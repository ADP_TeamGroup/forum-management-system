﻿using System;

namespace ForumSystem.Services.Exceptions
{
    public class InvalidInputException : ApplicationException
    {
        public InvalidInputException(string message) : base(message) { }
    }
}
