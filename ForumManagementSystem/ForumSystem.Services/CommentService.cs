﻿using ForumSystem.Data;
using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Mappers;
using ForumSystem.Data.Models.ViewModels;
using ForumSystem.Services.Exceptions;
using ForumSystem.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForumSystem.Services
{
    public class CommentService : ICommentService
    {
        private readonly ForumDbContext _database;
        private readonly CommentMapper _commentMapper;

        public CommentService(ForumDbContext database, CommentMapper commentMapper)
        {
            _database = database;
            _commentMapper = commentMapper;
        }

        public ResponseCommentDto GetById(User user, int id)
        {
            if (user.IsBlocked)
            {
                throw new UserAlreadyBlockedUnblocked("User is blocked!");
            }

            Comment comment = this._database.Comments
                                     .Include(comment => comment.User)
                                     .Include(comment => comment.Post)
                                     .FirstOrDefault(c => c.Id == id);
            var responseDto = this._commentMapper.ConvertToDto(comment);

            return responseDto;

        }
        public Comment Get(User user, int id)
        {
            if (user.IsBlocked)
            {
                throw new UserAlreadyBlockedUnblocked("User is blocked!");
            }

            var comment = this._database.Comments.Include(c => c.Post).Include(c=>c.User).Where(c => c.Id == id).FirstOrDefault();

            return comment ?? throw new CommentNotFoundException("Comment does not exist!");

        }

        public List<ResponseCommentDto> GetAllForPost(User user, CommentQueryParameters filterParameters)
        {
            if (user.IsBlocked)
            {
                throw new UserAlreadyBlockedUnblocked("User is blocked!");
            }

           int postId = filterParameters.PostId;
           string postTitle = !string.IsNullOrEmpty(filterParameters.PostTitle) ? filterParameters.PostTitle : string.Empty;

            List<Comment> result = _database.Comments.ToList();
            result = GetByPostId(result, postId);
            result = GetByPostTitle(result, postTitle);
            

            return result.Select(c => _commentMapper.ConvertToDto(c)).ToList();
           
        }

        public ResponseCommentDto Create(User user, CreateCommentDto commentDto)
        {
            if (user.IsBlocked)
            {
                throw new UserAlreadyBlockedUnblocked("User is blocked!");
            }

            var comment = this._commentMapper.ConvertToModel(commentDto);
            comment.UserId = user.Id;

            var postId = this._database.Posts.FirstOrDefault(p => p.Id == comment.PostId) ?? 
                throw new EntityNotFoundException($"Post with id {comment.PostId} doesn't exist!");
           

            var createdComment = this._database.Comments.Add(comment).Entity;

            this._database.SaveChanges();
            var responceComment = this._commentMapper.ConvertToDto(createdComment);
            return responceComment;
        }

        public Comment Create(Comment comment)
        {
            var createdComment = this._database.Comments.Add(comment).Entity;
            this._database.SaveChanges();
            return createdComment;
        }

        public ResponseCommentDto Update(int id, User user, UpdateCommentDto commentDto)
        {
            if (user.IsBlocked)
            {
                throw new UserAlreadyBlockedUnblocked("User is blocked!");
            }
            var foundComment = this.Get(user, id);
            foundComment.Content = commentDto.Content;
            this._database.SaveChanges();
            var responseComment = this._commentMapper.ConvertToDto(foundComment);

            return responseComment;
        }

        public Comment Update( CommentsModel viewModel )
        {
           
            var currentUser = _database.Users.FirstOrDefault(u => u.Username == viewModel.Username);
            var foundComment = this.Get(currentUser, viewModel.CommentId);

            if (foundComment.User.Username != currentUser.Username)
            {
                throw new UnauthorizedException("This comment is not yours and you are not authorized to delete it!");
            }
            if (currentUser.IsBlocked)
            {
                throw new UserAlreadyBlockedUnblocked("User is blocked!");
            }

            foundComment.Content = viewModel.Content;
            this._database.SaveChanges();

            return foundComment;
        }

        
        public Comment Delete(string username, int id)
        {
            var user = _database.Users.FirstOrDefault(u => u.Username == username);
            var comment = this.Get(user, id);

            if (comment.User.Username != user.Username && comment.User.IsAdmin == false)
            {
                throw new UnauthorizedException("This comment is not yours and you are not authorized to delete it!");
            }
            if (user.IsBlocked)
            {
                throw new UserAlreadyBlockedUnblocked("User is blocked!");
            }
            
            this._database.Comments.Remove(comment);
            _database.SaveChanges();

            return comment;
        }

        private List<Comment> GetByPostId(List<Comment> result, int postId)
        {
            if (postId == 0)
            {
                return result;
            }
            var comments = this._database.Comments
                                 .Include(comment => comment.Post)
                                 .Where(p => p.PostId == postId)
                                 .ToList();
            return comments;
        }

        private List<Comment> GetByPostTitle(List<Comment> result, string postTitle)
        {
            if (postTitle == string.Empty)
            {
                return result;
            }
            var comments = this._database.Comments
                                 .Include(comment => comment.Post)
                                 .Where(p => p.Post.Title == postTitle)
                                 .ToList();

            return comments;
        }

    }
}
