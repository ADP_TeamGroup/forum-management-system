﻿namespace ForumSystem.Services
{
    public static class Constants
    {
        public const string ErrorMessagePostVoteReaction = "Invalid vote reaction!";
        public const string AlreadyExistsVoteReaction = "Vote already set to {0}!";
    }
}
