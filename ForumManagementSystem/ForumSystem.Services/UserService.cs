﻿using ForumSystem.Data;
using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Mappers;
using ForumSystem.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ForumSystem.Services
{
    public class UserService : IUserService
    {
        private readonly ForumDbContext _database;
        private readonly UserMapper _userMapper;

        public UserService(ForumDbContext database, UserMapper userMapper)
        {
            _database = database;
            _userMapper = userMapper;
        }

        public User GetUserById(int id)
        {
            return _database.Users.FirstOrDefault(u => u.Id == id) ?? throw new UserNotFoundException("User not found!");
        }

        public User GetByUsername(string username)
        {
            return _database.Users.FirstOrDefault(u => u.Username == username) ?? throw new UserNotFoundException("User not found!");
        }

        public List<User> Get(UserQuerryParameters userParameter)
        {
            string username = userParameter.Username ?? string.Empty;
            string email = userParameter.Email ?? string.Empty;
            string firstName = userParameter.FirstName ?? string.Empty;
            string lastName = userParameter.LastName ?? string.Empty;

            List<User> result = _database.Users.ToList();
            result = GetByUsername(result, username);
            result = GetByEmail(result, email);
            result = GetByFirstName(result, firstName);
            result = GetByLastName(result, lastName);

            return result.Any() ? result.ToList() : throw new UserNotFoundException("User not found!");
        }

        public List<User> GetAll()
        {
            return _database.Users.Include(u => u.Posts).ToList();
        }

        public UserResponseDto Create(UserDto newUser)
        {
            var user = _userMapper.ConvertToUser(newUser);

            EmailExist(user.Email);
            UsernameExist(user.Username);

            _database.Users.Add(user);
            _database.SaveChanges();

            var responseUser = _userMapper.ConvertToResponseUser(user);

            return responseUser;
        }

        public UserResponseDto Update(User currentUser, UpdateUserDto updateUser)
        {
            currentUser.FirstName = updateUser.FirstName ?? currentUser.FirstName;
            currentUser.LastName = updateUser.LastName ?? currentUser.LastName;
            currentUser.Password = updateUser.Password ?? currentUser.Password;


            if (!string.IsNullOrEmpty(updateUser.Email) && updateUser.Email != currentUser.Email)
            {
                EmailExist(updateUser.Email);
                currentUser.Email = updateUser.Email;
            }

            currentUser.PhoneNumber = updateUser.PhoneNumber ?? currentUser.PhoneNumber;
            currentUser.IsBlocked = updateUser.IsBlocked;
            currentUser.IsAdmin = updateUser.IsAdmin;

            _database.Users.Update(currentUser);
            _database.SaveChanges();

            var updatedResponseUser = _userMapper.ConvertToResponseUser(currentUser);

            return updatedResponseUser;
        }

        public int GetRegisteredUsersCount()
        {
            return _database.Users.Count();
        }

        public void Delete(int userId)
        {
            var user = this.GetUserById(userId);

            _database.Users.Remove(user);
            _database.SaveChanges();
        }

        public User Authenticate(string username, string password)
        {
            var user = _database.Users.FirstOrDefault(u => u.Username == username
                                                      && u.Password == password);

            if (user != null)
            {
                return user;
            }

            throw new UserNotFoundException("User does not exist!");
        }
        public User Authenticate(string username)
        {
            var user = _database.Users.FirstOrDefault(u => u.Username == username);

            if (user != null)
            {
                return user;
            }

            throw new UserNotFoundException("User does not exist!");
        }

        public void ChangeRoleOrBlock(string username, string action, string phoneNumber)
        {
            var userQuerry = new UserQuerryParameters { Username = username };
            var searchUser = this.Get(userQuerry);

            var foundUser = searchUser[0];

            switch (action)
            {
                case "admin":
                    if (foundUser.IsAdmin)
                    {
                        throw new UserIsAdminException("This user is already Admin!");
                    }

                    foundUser.IsAdmin = true;
                    foundUser.PhoneNumber = phoneNumber;
                    break;
                case "block":
                    if (foundUser.IsAdmin)
                    {
                        throw new UserIsAdminException("This user is Admin and can't be blocked!");
                    }
                    else if (foundUser.IsBlocked)
                    {
                        throw new UserAlreadyBlockedUnblocked("This user is already blocked!");
                    }

                    foundUser.IsBlocked = true;
                    break;
                case "unblock":
                    if (foundUser.IsAdmin)
                    {
                        throw new UserIsAdminException("This user is Admin and can't be unblocked!");
                    }
                    else if (!foundUser.IsBlocked)
                    {
                        throw new UserAlreadyBlockedUnblocked("This user is already unblocked!");
                    }

                    foundUser.IsBlocked = false;
                    break;
                default:
                    throw new InvalidInputException("Invalid input for action / 'block', 'unblock' or 'admin'!");
            }

            _database.Users.Update(foundUser);
            _database.SaveChanges();
        }



        //Private methods
        private static List<User> GetByUsername(List<User> result, string username)
        {
            return result.Where(user => user.Username.Contains(username, StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        private static List<User> GetByEmail(List<User> result, string email)
        {
            return result.Where(user => user.Email.Contains(email, StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        private static List<User> GetByFirstName(List<User> result, string firstName)
        {
            return result.Where(user => user.FirstName.Contains(firstName, StringComparison.InvariantCultureIgnoreCase)).ToList();
        }

        private static List<User> GetByLastName(List<User> result, string lastName)
        {
            return result.Where(user => user.LastName.Contains(lastName, StringComparison.InvariantCultureIgnoreCase)).ToList();

        }

        private void EmailExist(string email)
        {
            var emailExist = _database.Users.FirstOrDefault(ue => ue.Email == email);

            if (emailExist != null)
            {
                throw new AlreadyExistsException("Email already exists!");
            }
        }

        private void UsernameExist(string username)
        {
            var usernameExist = _database.Users.FirstOrDefault(ue => ue.Username == username);

            if (usernameExist != null)
            {
                throw new AlreadyExistsException("Username already exists!");
            }
        }
    }
}
