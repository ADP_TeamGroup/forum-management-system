﻿using ForumSystem.Data;
using ForumSystem.Data.Models;
using System;
using System.Linq;


namespace ForumSystem.Services
{
    public class TagService : ITagService
    {
        private readonly ForumDbContext _database;

        public TagService(ForumDbContext database)
        {
            _database = database;
        }

        public Tag Create(string tagName)
        {
            var tag = CheckIfTagExist(tagName);

            if (tag != null)
            {
                return tag;
            }

            tag = new Tag { TagContent = tagName };

            _database.Tags.Add(tag);
            _database.SaveChanges();

            return tag;
        }

        private Tag CheckIfTagExist(string tagName)
        {
            return _database.Tags.FirstOrDefault(t => t.TagContent == tagName);
        }
    }
}
