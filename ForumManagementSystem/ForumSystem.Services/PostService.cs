﻿using ForumSystem.Data;
using ForumSystem.Data.Models;
using ForumSystem.Data.Models.Dto_s;
using ForumSystem.Data.Models.Enums;
using ForumSystem.Data.Models.Mappers;
using ForumSystem.Data.Models.QueryParams;
using ForumSystem.Data.Models.ViewModels;
using ForumSystem.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ForumSystem.Services.Posts
{
    public class PostService : IPostService
    {
        private readonly PostMapper _postMapper;
        private readonly ForumDbContext _database;

        public PostService(ForumDbContext database, PostMapper postMapper)
        {
            _database = database;
            _postMapper = postMapper;
        }

        public List<Post> GetAll()
        {
            return _database.Posts
                   .Include(p => p.User)
                   .Include(p => p.Tags)
                   .ThenInclude(t => t.Tag).ToList();
        }

        public List<PostResponseDto> GetAllPostResponseDtos()
        {
            var result = _database.Posts.Include(p => p.User).Include(p => p.Tags).ThenInclude(t => t.Tag);

            return result.Select(p => _postMapper.ConvertToResponseDto(p)).ToList();
        }

        public List<PostResponseDto> Get(PostQueryParameters postQueryParameters)
        {
            string sortCriteria = postQueryParameters.SortBy ?? string.Empty;
            string username = postQueryParameters.Username ?? string.Empty;
            string tag = postQueryParameters.Tag ?? string.Empty;


            IQueryable<Post> result = _database.Posts
                                     .Include(u => u.User)
                                     .Include(postTags => postTags.Tags)
                                     .ThenInclude(t => t.Tag);


            result = SortBy(result, sortCriteria);
            result = FilterByUsername(result, username);
            result = FilterByTag(result, tag);

            return result.Select(p => _postMapper.ConvertToResponseDto(p)).ToList();
        }

        public Post GetById(int id)
        {
            if (!_database.Posts.Any(p => p.Id == id))
            {
                throw new PostNotFoundException($"Post with id: {id} doesn't exist!");
            }
            var post = _database.Posts
                        .Include(p => p.User)
                        .Include(p => p.Comments)
                            .ThenInclude(u=> u.User)
                        .Include(p => p.Votes)
                        .Include(t => t.Tags)
                            .ThenInclude(t => t.Tag)
                        .FirstOrDefault(p => p.Id == id);
            return post;
        }

        public Post GetByTitle(string title)
        {
            if (!_database.Posts.Any(p => p.Title == title))
            {
                throw new PostNotFoundException($"Post with title: '{title}' doesn't exist!");
            }

            var post = _database.Posts.Include(p => p.User).FirstOrDefault(p => p.Title == title);
            return post;
        }

        public Dictionary<int, List<string>> GetPostTagsContent(IEnumerable<Post> posts)
        {
            Dictionary<int, List<string>> postsIdAndTags = new Dictionary<int, List<string>>();
            foreach (var post in posts)
            {
                var tags = new List<string>();
                foreach (var tag in post.Tags)
                {
                    tags.Add(tag.Tag.TagContent);
                }
                postsIdAndTags.Add(post.Id, tags);
            }

            return postsIdAndTags;
        }

        public PostResponseDto Create(User user, PostDto postDto)
        {
            if (user.IsBlocked)
            {
                throw new UserAlreadyBlockedUnblocked("User is blocked!");
            }
            if (_database.Posts.Any(p => p.Title == postDto.Title))
            {
                throw new AlreadyExistsException($"Post with title: {postDto.Title} already exists");
            }

            var post = _postMapper.ConvertToPost(user, postDto);
            _database.Posts.Add(post);
            _database.SaveChanges();

            var postResponseDto = _postMapper.ConvertToResponseDto(post);
            return postResponseDto;
        }

        public PostResponseDto Update(int postToUpdateId, UpdatePostDto postDto, User currentUser)
        {
            var postToUpdate = GetById(postToUpdateId);

            if (!_database.Posts.Any(p => p.User == currentUser && p.Id == postToUpdateId))
            {
                throw new UnauthorizedException("You can't edit this post!");
            }

            var tag = GetTagByContent(postDto.Tag.ToLower()) ?? CreateTag(postDto.Tag.ToLower());

            if (!_database.Tags.Contains(tag))
            {
                _database.Tags.Add(tag);
            }

            if (_database.PostTags.Any(postTag => postTag.PostId == postToUpdate.Id && postTag.TagId == tag.Id))
            {
                throw new AlreadyExistsException($"Tag: {tag.TagContent} already exist for this post!");
            }

            postToUpdate.Title = postDto.Title ?? postToUpdate.Title;
            postToUpdate.Content = postDto.Content ?? postToUpdate.Content;

            var postTag = CreatePostTag(postToUpdate, tag);

            _database.PostTags.Add(postTag);
            _database.SaveChanges();


            postToUpdate.Tags = GetAllTagsForPost(postToUpdate);
            var postResponseDto = _postMapper.ConvertToResponseDto(postToUpdate);
            return postResponseDto;
        }

        public PostResponseDto EditPost(PostEditModel postEditModel)
        {
            var postToUpdate = GetById(postEditModel.Id);
            postToUpdate.Title = postEditModel.Title;
            postToUpdate.Content = postEditModel.Content;
            var tag = _database.Tags.FirstOrDefault(tag => tag.TagContent == postEditModel.Tag)
                                                ?? new Tag { TagContent = postEditModel.Tag };
            postToUpdate.Tags.Add(new PostTag
            {
                Post = postToUpdate,
                PostId = postToUpdate.Id,
                Tag = tag,
                TagId = tag.Id
            });

            _database.Update(postToUpdate);
            _database.SaveChanges();

            PostResponseDto postResponseDto = _postMapper.ConvertToResponseDto(postToUpdate);
            return postResponseDto;

        }

        public void Delete(string username, int postId, bool isAdmin)
        {
            var postToDelete = this.GetById(postId);

            if (!isAdmin)
            {
                if (postToDelete.User.Username != username)
                {
                    throw new UnauthorizedException("This post is not yours and you are not authorized to delete it!");
                }
            }

            _database.Posts.Remove(postToDelete);
            _database.SaveChanges();
        }

        public void Delete(string username, int postId)
        {
            var postToDelete = this.GetById(postId);
            
            if (postToDelete.User.Username != username )
            {
                throw new UnauthorizedException("This post is not yours and you are not authorized to delete it!");
            }

             RemoveAllRealtioshipsOfPost(postToDelete);
            _database.Posts.Remove(postToDelete);
            _database.SaveChanges();
        }

        public void Delete( int postId)
        {
            var postToDelete = this.GetById(postId);
            
            RemoveAllRealtioshipsOfPost(postToDelete);
            _database.Posts.Remove(postToDelete);
            _database.SaveChanges();
        }

        public IEnumerable<Post> GetFilteredPosts(string option,string input)
        {
            var postQueryParams = new PostViewQueryParamenters();

            if (string.IsNullOrEmpty(option))
            {
                return GetAll().AsEnumerable();
            }

            switch (option.ToLower())
            {
                case "title":
                    postQueryParams.Title = input;
                    break;
                case "username":
                    postQueryParams.Username = input;
                    break;
                case "tag":
                    postQueryParams.Tag = input;
                    break;
                default:
                    throw new PostNotFoundException("Invalid search criteria!");
            }
            IEnumerable<Post> filteredPosts = FilterPosts(postQueryParams);
            return filteredPosts;
        }

        private IEnumerable<Post> FilterPosts(PostViewQueryParamenters postQueryParams)
        {
            string title = postQueryParams.Title ?? string.Empty;
            string username = postQueryParams.Username ?? string.Empty;
            string tag = postQueryParams.Tag ?? string.Empty;

            IEnumerable<Post> result = _database.Posts
                .Include(post => post.User)
                .Include(post => post.Tags)
                    .ThenInclude(tag => tag.Tag);

            result = GetByTitle(result, title);
            result = GetByUsername(result, username);
            result = GetByTag(result, tag);

            return result.Any() ? result : throw new PostNotFoundException("Posts not found!");
        }

        private IEnumerable<Post> GetByTag(IEnumerable<Post> result, string tag)
        {
            if (string.IsNullOrEmpty(tag))
            {
                return result;
            }

            var postTags = _database.PostTags
                .Include(t => t.Tag)
                .Include(t => t.Post)
                    .ThenInclude(p => p.Tags)
                        .ThenInclude(t => t.Tag)
                .Include(t => t.Post)
                    .ThenInclude(p => p.User)
                .Where(pt => pt.Tag.TagContent == tag);
            result= postTags.Select(p => p.Post);
            return result;
        }

        private IEnumerable<Post> GetByUsername(IEnumerable<Post> result, string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return result;
            }
            return result.Where(p => p.User.Username.Contains(username));
        }

        private IEnumerable<Post> GetByTitle(IEnumerable<Post> result, string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                return result;
            }
           
            return result.Where(p => p.Title.Contains( title));
        }

        public List<PostResponseDto> RecentlyCreated()
        {
            var allPosts = this.GetAll().TakeLast(10).Select(p => _postMapper.ConvertToResponseDto(p));

            return allPosts.ToList();
        }

        public List<PostResponseDto> MostCommented()
        {
            var mostCommented = _database.Posts
                .Include(p => p.User)
                .Include(p => p.Tags).ThenInclude(t => t.Tag)
                .OrderByDescending(p => p.Comments.Count)
                                               .Take(10)
                                               .Select(p => _postMapper.ConvertToResponseDto(p));

            return mostCommented.ToList();

        }


        public PostResponseDto UpdateLikes(int id,
                                           User currentUser,
                                           PostVoteQueryParameters postVoteQuery)
        {
            var postToUpdateLikes = GetById(id);
            var updatedPost = GetVote(currentUser, postToUpdateLikes, postVoteQuery);

            _database.Posts.Update(updatedPost);
            _database.SaveChanges();

            return _postMapper.ConvertToResponseDto(updatedPost);
        }

        public PostResponseDto UpdateLikes(User currentUser, string reaction)
        {
            string[] reactionAndPostId = reaction.Split(',');
            var vote = reactionAndPostId[0] == "Like" ? Reaction.Like : Reaction.Dislike;

            var id = int.Parse(reactionAndPostId[1]);
            var postToUpdateLikes = GetById(id);
            var updatedPost = GetVote(currentUser, postToUpdateLikes, vote);

            _database.Posts.Update(updatedPost);
            _database.SaveChanges();

            var postResponse = _postMapper.ConvertToResponseDto(updatedPost);
            return postResponse;

        }

        private Post GetVote(User currentUser, Post postToUpdateLikes, Reaction reaction)
        {
            var userPostReaction = _database.PostLikes.FirstOrDefault(p => p.User.Equals(currentUser) && p.Post.Equals(postToUpdateLikes))
           ?? new PostLike
           {
               UserId = currentUser.Id,
               User = currentUser,
               PostId = postToUpdateLikes.Id,
               Post = postToUpdateLikes,
               Vote = Reaction.Neutral
           };
            if (userPostReaction.Vote == Data.Models.Enums.Reaction.Neutral)
            {
                switch (reaction.ToString().ToLower())
                {
                    case "like":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Like;
                        postToUpdateLikes.Likes++;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                    case "dislike":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Dislike;

                        postToUpdateLikes.Likes--;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                }
                return postToUpdateLikes;

            }
            if (userPostReaction.Vote == Data.Models.Enums.Reaction.Like)
            {
                switch (reaction.ToString().ToLower())
                {
                    case "like":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Neutral;
                        postToUpdateLikes.Likes--;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                    case "dislike":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Dislike;

                        postToUpdateLikes.Likes -= 2;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                }
                return postToUpdateLikes;

            }
            if (userPostReaction.Vote == Data.Models.Enums.Reaction.Dislike)
            {
                switch (reaction.ToString().ToLower())
                {
                    case "like":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Like;
                        postToUpdateLikes.Likes += 2;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                    case "dislike":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Neutral;

                        postToUpdateLikes.Likes++;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                }
                return postToUpdateLikes;

            }
            return postToUpdateLikes;

        }

        public PostResponseDto DeleteTag(int postId, User currentUser, TagQueryParameters tagQuery)
        {
            var post = GetById(postId);
            if (!currentUser.IsAdmin)
            {
                if (currentUser != post.User)
                {
                    throw new UnauthorizedException("You can't delete post tag of other user post!");
                }
            }
            var tagToDelete = GetTagByContent(tagQuery.Tag);
            var postTag = _database.PostTags.Include(t => t.Tag).FirstOrDefault(t => t.Tag == tagToDelete);
            post.Tags.Remove(postTag);
            _database.PostTags.Remove(postTag);
            _database.SaveChanges();

            return _postMapper.ConvertToResponseDto(post);
        }




        //Private methods
        private void RemoveAllRealtioshipsOfPost(Post postToDelete)
        {
            DeletePostLikesForPost(postToDelete);
        }

        private void DeletePostLikesForPost(Post postToDelete)
        {
            var postLikesToRemove = GetAllPostLikesForPost(postToDelete);
            _database.PostLikes.RemoveRange(postLikesToRemove);
            _database.SaveChanges();
        }

        private PostLike[] GetAllPostLikesForPost(Post postToDelete)
        {
            return postToDelete.Votes.Where(p => p.Post == postToDelete).ToArray();
        }

        private List<PostTag> GetAllTagsForPost(Post post)
        {
            var postTags = _database.PostTags.Include("Tag").Where(p => p.PostId == post.Id).ToList();
            return postTags;
        }

        private static Tag CreateTag(string tagContent)
        {
            var tag = new Tag
            {
                TagContent = tagContent
            };
            return tag;
        }

        private static PostTag CreatePostTag(Post postToUpdate, Tag tag)
        {
            var postTag = new PostTag
            {
                PostId = postToUpdate.Id,
                Post = postToUpdate,
                TagId = tag.Id,
                Tag = tag,
            };

            return postTag;
        }

        private Tag GetTagByContent(string tagContent)
        {
            return _database.Tags.FirstOrDefault(t => t.TagContent == tagContent);
        }

        private IQueryable<Post> FilterByTag(IQueryable<Post> result, string tagName)
        {
            var tag = GetTagByContent(tagName.ToLower());
            var postTags = _database.PostTags.Include("Tag");
            postTags = postTags.Where(p => p.TagId == tag.Id);

            return result.Where(p => postTags.Select(i => i.Post).Contains(p));
        }

        private IQueryable<Post> SortBy(IQueryable<Post> result, string sortCriteria)
        {
            return sortCriteria.ToLowerInvariant() switch
            {
                "usernameasc" => _database.Posts.Include("User").OrderBy(p => p.User.Username),
                "usernamedesc" => _database.Posts.Include("User").OrderByDescending(p => p.User.Username),
                "likesasc" => _database.Posts.Include("User").OrderBy(p => p.Likes),
                "likesdesc" => _database.Posts.Include("User").OrderByDescending(p => p.Likes),
                "titleasc" => _database.Posts.Include("User").OrderBy(p => p.Title),
                "titledesc" => _database.Posts.Include("User").OrderByDescending(p => p.Title),
                _ => result,
            };
        }

        private static IQueryable<Post> FilterByUsername(IQueryable<Post> result, string username)
        {
            if (username == String.Empty)
            {
                return result;
            }

            var posts = result.Include("User").Where(p => p.User.Username.Contains(username, StringComparison.InvariantCultureIgnoreCase));

            return posts;
        }

        private Post GetVote(User currentUser, Post postToUpdateLikes, PostVoteQueryParameters postQueryParameters)
        {
            var userPostReaction = _database.PostLikes.FirstOrDefault(p => p.User.Equals(currentUser) && p.Post.Equals(postToUpdateLikes))
            ?? new PostLike
            {
                UserId = currentUser.Id,
                User = currentUser,
                PostId = postToUpdateLikes.Id,
                Post = postToUpdateLikes,
                Vote = Data.Models.Enums.Reaction.Neutral
            };

            if (userPostReaction.Vote == Data.Models.Enums.Reaction.Neutral)
            {
                switch (postQueryParameters.Vote.ToLower())
                {
                    case "like":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Like;
                        postToUpdateLikes.Likes++;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                    case "dislike":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Dislike;

                        postToUpdateLikes.Likes--;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                    case "neutral":
                        string message = String.Format(Constants.AlreadyExistsVoteReaction, "neutral");
                        throw new AlreadyExistsException(message);
                    default:
                        string ErrorMessage = Constants.ErrorMessagePostVoteReaction;
                        throw new InvalidInputException(ErrorMessage);
                }
            }
            else if (userPostReaction.Vote == Data.Models.Enums.Reaction.Dislike)
            {
                switch (postQueryParameters.Vote.ToLower())
                {
                    case "like":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Like;
                        postToUpdateLikes.Likes += 2;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                    case "neutral":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Neutral;
                        postToUpdateLikes.Likes++;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                    case "dislike":
                        string message = String.Format(Constants.AlreadyExistsVoteReaction, "dislike");
                        throw new AlreadyExistsException(message);
                    default:
                        string ErrorMessage = Constants.ErrorMessagePostVoteReaction;
                        throw new InvalidInputException(ErrorMessage);
                }
            }
            else
            {
                switch (postQueryParameters.Vote.ToLower())
                {
                    case "dislike":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Dislike;
                        postToUpdateLikes.Likes -= 2;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                    case "neutral":
                        userPostReaction.Vote = Data.Models.Enums.Reaction.Neutral;

                        postToUpdateLikes.Likes--;

                        postToUpdateLikes.Votes.Remove(userPostReaction);
                        postToUpdateLikes.Votes.Add(userPostReaction);
                        break;
                    case "like":
                        string message = String.Format(Constants.AlreadyExistsVoteReaction, "like");
                        throw new AlreadyExistsException(message);
                    default:
                        string ErrorMessage = Constants.ErrorMessagePostVoteReaction;
                        throw new InvalidInputException(ErrorMessage);

                }
            }
            return postToUpdateLikes;
        }



    }
}
